## Insoundious

[![Build status](https://build.appcenter.ms/v0.1/apps/1e26a00f-7cd2-4e44-a0ff-6048b498e4c8/branches/release%2Fv1.0.0-beta.1/badge)](https://appcenter.ms)

## keystore & password

- What is your first and last name?
  [Unknown]: Abdul Fattah Ikhsan

- What is the name of your organizational unit?
  [Unknown]:
- What is the name of your organization?
  [Unknown]:
- What is the name of your City or Locality?
  [Unknown]: Jakarta
- What is the name of your State or Province?
  [Unknown]: Jakarta
- What is the two-letter country code for this unit?
  [Unknown]: ID
- Is CN=Abdul Fattah Ikhsan, OU=Unknown, O=Unknown, L=Jakarta, ST=Jakarta, C=ID correct? [yes]
- pass = insoundious
