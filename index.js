/**
 * @format
 */
import 'react-native-gesture-handler';

import {AppRegistry} from 'react-native';
import TrackPlayer from 'react-native-track-player';
// import App from './App';
import App from './src/App';
import AppService from './src/services/AppService';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
TrackPlayer.registerPlaybackService(() => AppService.playerListener);
