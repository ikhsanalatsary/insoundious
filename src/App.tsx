/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState, useCallback} from 'react';
import codePush from 'react-native-code-push';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {
  Provider as PaperProvider,
  ActivityIndicator,
  Snackbar,
} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import TrackPlayer, {TrackPlayerEvents} from 'react-native-track-player';
import Router from './Router';
import {theme, darkTheme, navTheme, navDarkTheme} from './themes';
import {YellowBox, StatusBar} from 'react-native';
import AppService from './services/AppService';
import {SafeAreaLayout, SaveAreaInset} from './components/SafeAreaLayout';
import {
  SHOW_SNACK_BAR,
  useAppState,
  AppProvider,
  useAppDispatch,
  ERROR_MESSAGE,
  CHANGE_THEME,
} from './Context';
import Config from 'react-native-config';

YellowBox.ignoreWarnings([
  'Require cycle:',
  'VirtualizedLists should never be nested', // TODO: Remove when fixed
]);

// let isDarkMode = true;
let version = require('../app.json').version;

function App() {
  useEffect(
    useCallback(() => {
      codePush.sync({
        deploymentKey:
          version.search(/beta/) === -1
            ? Config.PRODUCTION_DEPLOYMENT_KEY
            : Config.STAGING_DEPLOYMENT_KEY,
        installMode: codePush.InstallMode.ON_NEXT_RESUME,
      });
      // TODO: change to static in the future
      AsyncStorage.getItem('_insoundious_track_').then(foo => {
        if (!foo) {
          TrackPlayer.setupPlayer().then(() =>
            AsyncStorage.setItem('_insoundious_track_', 'setup_ready'),
          );
        }
      });
    }, []),
  );

  return (
    <SafeAreaProvider>
      <AppProvider>
        <Main />
      </AppProvider>
    </SafeAreaProvider>
  );
}

function Main() {
  let {isDarkMode, visible, message} = useAppState();
  let [isDBReady, setDBReady] = useState(false);
  let appDispatch = useAppDispatch();
  TrackPlayer.useTrackPlayerEvents(
    [TrackPlayerEvents.PLAYBACK_ERROR],
    async event => {
      if (event.type === TrackPlayerEvents.PLAYBACK_ERROR) {
        appDispatch({
          type: SHOW_SNACK_BAR,
          message: ERROR_MESSAGE,
        });
      }
    },
  );

  useEffect(() => {
    TrackPlayer.updateOptions({
      stopWithApp: true,
      capabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
        TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
        TrackPlayer.CAPABILITY_STOP,
      ],
      compactCapabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
      ],
    });
    AppService.storageService.getSetting().then(res => {
      appDispatch({type: CHANGE_THEME, isDarkMode: res.isDarkMode});
    });
    AppService.openDatabase
      .then(() => {
        setDBReady(true);
      })
      .catch(console.log);
  }, [appDispatch]);
  // let backgroundColor = isDarkMode
  //   ? overlay(elevation, darkTheme.colors.surface)
  //   : theme.colors.primary;
  // StatusBar.setBackgroundColor(backgroundColor);
  StatusBar.setTranslucent(true);
  return (
    <PaperProvider theme={isDarkMode ? darkTheme : theme}>
      <Snackbar
        visible={visible}
        onDismiss={() => appDispatch({type: SHOW_SNACK_BAR, message: ''})}>
        {message}
      </Snackbar>
      {!isDBReady ? (
        <SafeAreaLayout
          style={{flex: 1, justifyContent: 'center'}}
          insets={SaveAreaInset.TOP}>
          <ActivityIndicator size="large" animating={true} />
        </SafeAreaLayout>
      ) : (
        // @ts-ignore
        <Router theme={isDarkMode ? navDarkTheme : navTheme} />
      )}
    </PaperProvider>
  );
}

// function Root() {
//   let [isDBReady, setDBReady] = useState(false);
//   const {visible, message} = useSnackBarState();
//   const appDispatch = useSnackBarDispatch();

//   useEffect(() => {
//     AppService.openDatabase
//       .then(() => {
//         setDBReady(true);
//       })
//       .catch(console.log);
//     TrackPlayer.setupPlayer();
//     TrackPlayer.updateOptions({
//       stopWithApp: true,
//       capabilities: [
//         TrackPlayer.CAPABILITY_PLAY,
//         TrackPlayer.CAPABILITY_PAUSE,
//         TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
//         TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
//         TrackPlayer.CAPABILITY_STOP,
//       ],
//       compactCapabilities: [
//         TrackPlayer.CAPABILITY_PLAY,
//         TrackPlayer.CAPABILITY_PAUSE,
//       ],
//     });
//   }, []);

//   return (
//     <>
//       <Snackbar
//         visible={visible}
//         onDismiss={() => appDispatch({type: SHOW_SNACK_BAR, message: ''})}
//         // action={{
//         //   label: 'Undo',
//         //   onPress: () => {
//         //     // Do something
//         //   },
//         // }}
//       >
//         {message}
//       </Snackbar>
//       {!isDBReady ? (
//         <SafeAreaLayout
//           style={{flex: 1, justifyContent: 'center'}}
//           insets={SaveAreaInset.TOP}>
//           <ActivityIndicator size="large" animating={true} />
//         </SafeAreaLayout>
//       ) : (
//         <Router />
//       )}
//     </>
//   );
// }

const {CheckFrequency} = codePush;
export default codePush({
  checkFrequency: __DEV__ ? CheckFrequency.MANUAL : CheckFrequency.ON_APP_START,
})(App);
