import React from 'react';

type Props = {
  children: React.ReactNode;
};
type TAction = {type: typeof CHANGE_THEME};
type SAction = {type: typeof SHOW_SNACK_BAR; message: string};
type TDispatch = (action: TAction) => void;
type SDispatch = (action: SAction) => void;
type TState = {isDarkMode: boolean};
type SState = {visible: boolean; message: string};
type Action =
  | {type: typeof CHANGE_THEME; isDarkMode: boolean}
  | {type: typeof SHOW_SNACK_BAR; message: string};
type Dispatch = (action: Action) => void;
type State = {visible: boolean; message: string; isDarkMode: boolean};

const AppContext = React.createContext<State | undefined>(undefined);
const AppDispatchContext = React.createContext<Dispatch | undefined>(undefined);
const ThemeContext = React.createContext<TState | undefined>(undefined);
const ThemeDispatchContext = React.createContext<TDispatch | undefined>(
  undefined,
);
const CHANGE_THEME = 'CHANGE_THEME';
const SnackBarContext = React.createContext<SState | undefined>(undefined);
const SnackBarDispatchContext = React.createContext<SDispatch | undefined>(
  undefined,
);
const SHOW_SNACK_BAR = 'SHOW_SNACK_BAR';
const ERROR_MESSAGE = 'Alfred says something is not right';

function themeReducer(state: TState, action: TAction) {
  switch (action.type) {
    case CHANGE_THEME: {
      return {isDarkMode: !state.isDarkMode};
    }

    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

function ThemeProvider({children}: Props) {
  const [state, dispatch] = React.useReducer(themeReducer, {isDarkMode: false});

  return (
    <ThemeContext.Provider value={state}>
      <ThemeDispatchContext.Provider value={dispatch}>
        {children}
      </ThemeDispatchContext.Provider>
    </ThemeContext.Provider>
  );
}

function useThemeState() {
  const context = React.useContext(ThemeContext);

  if (context === undefined) {
    throw new Error('useThemeState must be used within a ThemeProvider');
  }

  return context;
}

function useThemeDispatch() {
  const context = React.useContext(ThemeDispatchContext);

  if (context === undefined) {
    throw new Error('useThemeDispatch must be used within a ThemeProvider');
  }

  return context;
}

function snackBarReducer(state: SState, action: SAction) {
  switch (action.type) {
    case SHOW_SNACK_BAR: {
      return {visible: !state.visible, message: action.message};
    }

    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

function SnackBarProvider({children}: Props) {
  const [state, dispatch] = React.useReducer(snackBarReducer, {
    visible: false,
    message: '',
  });

  return (
    <SnackBarContext.Provider value={state}>
      <SnackBarDispatchContext.Provider value={dispatch}>
        {children}
      </SnackBarDispatchContext.Provider>
    </SnackBarContext.Provider>
  );
}

function useSnackBarState() {
  const context = React.useContext(SnackBarContext);

  if (context === undefined) {
    throw new Error('useSnackBarState must be used within a SnackBarProvider');
  }

  return context;
}

function useSnackBarDispatch() {
  const context = React.useContext(SnackBarDispatchContext);

  if (context === undefined) {
    throw new Error(
      'useSnackBarDispatch must be used within a SnackBarProvider',
    );
  }

  return context;
}

function appReducer(state: State, action: Action) {
  switch (action.type) {
    case CHANGE_THEME: {
      console.log('state');
      return {...state, isDarkMode: action.isDarkMode};
    }

    case SHOW_SNACK_BAR: {
      return {...state, visible: !state.visible, message: action.message};
    }

    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

function AppProvider({children}: Props) {
  const [state, dispatch] = React.useReducer(appReducer, {
    isDarkMode: true,
    visible: false,
    message: '',
  });

  return (
    <AppContext.Provider value={state}>
      <AppDispatchContext.Provider value={dispatch}>
        {children}
      </AppDispatchContext.Provider>
    </AppContext.Provider>
  );
}

function useAppState() {
  const context = React.useContext(AppContext);

  if (context === undefined) {
    throw new Error('useAppState must be used within a AppProvider');
  }

  return context;
}

function useAppDispatch() {
  const context = React.useContext(AppDispatchContext);

  if (context === undefined) {
    throw new Error('useAppDispatch must be used within a AppProvider');
  }

  return context;
}

export {
  ThemeProvider,
  SnackBarProvider,
  CHANGE_THEME,
  SHOW_SNACK_BAR,
  useThemeState,
  useThemeDispatch,
  useSnackBarState,
  useSnackBarDispatch,
  AppProvider,
  useAppState,
  useAppDispatch,
  ERROR_MESSAGE,
};
