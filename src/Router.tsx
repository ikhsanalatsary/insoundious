import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
  NavigationContainer,
  NavigationContainerRef,
  NavigationState,
} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Home from './screens/Home';
// import Playlist from './screens/Playlist';
import Queue from './screens/Queue';
import TrackDetails from './screens/TrackDetails';
import createBottomTabNavigator from './components/MyBottomTabBar';
import Search from './screens/Search';
import {OmitMethodTrack} from './domains/Track';
import PlayerDetail from './screens/PlayerDetail';
import {State} from 'react-native-track-player';
import Library from './screens/Library';
import Artist from './screens/Artist';
import {BackHandler} from 'react-native';
import AppService from './services/AppService';
import LastPlayed from './screens/LastPlayed';
import PlayByArtist from './screens/PlayByArtist';
import About from './screens/About';
import {Theme} from '@react-navigation/native/lib/typescript/src/types';

type TrackDetailParams = {
  videoId: string;
  data: OmitMethodTrack;
  request: boolean;
};

type LastPlayedParams = {
  title: string;
};

export type StackParamList = {
  Home: undefined;
  TrackDetails: TrackDetailParams;
  LastPlayed: LastPlayedParams;
  PlayByArtist: {
    author: string;
    authorId: string;
  };
};

export type SearchStackParamList = {
  Search: undefined;
  TrackDetails: TrackDetailParams;
  PlayByArtist: {
    author: string;
    authorId: string;
  };
};

export type LibraryStackParamList = {
  Library: undefined;
  // Playlist: undefined;
  TrackDetails: TrackDetailParams;
  Artist: undefined;
  LastPlayed: LastPlayedParams;
  PlayByArtist: {
    author: string;
    authorId: string;
  };
  Queue: undefined;
  About: undefined;
};

export type RootStackParamList = {
  Main: undefined;
  Player: {playerState: State};
};

export type TabParamList = {
  Home: undefined;
  Search: undefined;
  Library: undefined;
};

const Stack = createStackNavigator<StackParamList>();
const SearchStack = createStackNavigator<SearchStackParamList>();
const LibraryStack = createStackNavigator<LibraryStackParamList>();
const Tab = createBottomTabNavigator<TabParamList>();
const RootStack = createStackNavigator<RootStackParamList>();

function StackRouter() {
  return (
    <Stack.Navigator initialRouteName="Home" headerMode="none">
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="TrackDetails" component={TrackDetails} />
      <Stack.Screen name="LastPlayed" component={LastPlayed} />
      <Stack.Screen name="PlayByArtist" component={PlayByArtist} />
    </Stack.Navigator>
  );
}

function SearchStackRouter() {
  return (
    <SearchStack.Navigator initialRouteName="Search" headerMode="none">
      <SearchStack.Screen name="Search" component={Search} />
      <SearchStack.Screen name="TrackDetails" component={TrackDetails} />
      <SearchStack.Screen name="PlayByArtist" component={PlayByArtist} />
    </SearchStack.Navigator>
  );
}

function LibraryStackRouter() {
  return (
    <LibraryStack.Navigator initialRouteName="Library" headerMode="none">
      <LibraryStack.Screen name="Library" component={Library} />
      {/* <LibraryStack.Screen name="Playlist" component={Playlist} /> */}
      <LibraryStack.Screen name="TrackDetails" component={TrackDetails} />
      <LibraryStack.Screen name="Artist" component={Artist} />
      <LibraryStack.Screen name="LastPlayed" component={LastPlayed} />
      <LibraryStack.Screen name="PlayByArtist" component={PlayByArtist} />
      <LibraryStack.Screen name="Queue" component={Queue} />
      <LibraryStack.Screen name="About" component={About} />
    </LibraryStack.Navigator>
  );
}

const MainRouter = function MainRouter() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={({route}) => ({
        tabBarIcon: ({
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          focused,
          color,
          size,
        }: {
          focused: boolean;
          color: string;
          size: number;
        }) => {
          let iconName = '';

          if (route.name === 'Home') {
            iconName = 'home';
          } else if (route.name === 'Search') {
            iconName = 'magnify';
          } else if (route.name === 'Library') {
            iconName = 'library-music';
          }

          // You can return any component that you like here!
          return <Icon name={iconName} size={size} color={color} />;
        },
      })}
      // tabBarOptions={{
      // activeTintColor: theme.colors.primary,
      // inactiveTintColor: theme.colors.accent,
      // style: {
      // backgroundColor: theme.colors.surface,
      // borderTopColor: theme.colors.surface,
      // elevation: 0,
      // shadowColor: 'none',
      // },
      // }}
    >
      <Tab.Screen
        name="Home"
        component={StackRouter} /* options={{tabBarVisible: true}} */
      />
      <Tab.Screen name="Search" component={SearchStackRouter} />
      <Tab.Screen name="Library" component={LibraryStackRouter} />
    </Tab.Navigator>
  );
};

const RootStackScreen = React.memo(() => (
  <RootStack.Navigator
    mode="modal"
    screenOptions={() => ({
      headerShown: false,
    })}>
    <RootStack.Screen name="Main" component={MainRouter} />
    <RootStack.Screen name="Player" component={PlayerDetail} />
  </RootStack.Navigator>
));

export default function Router({theme}: {theme: Theme}) {
  const ref = React.useRef<NavigationContainerRef>(null);
  const cb = React.useCallback(() => {
    // console.log(
    //   'ref.current ',
    //   JSON.stringify(ref.current?.getRootState(), null, 2),
    // );
    // let nav: NavigationState | undefined = ref.current?.getRootState();
    // console.log('nav index ', nav?.index);
    // console.log('nav routes ', nav?.routes);
    // console.log('nav routes ', nav?.routes[0].state);
    // console.log('nav ,,', nav?.routes[nav?.index]);
    AppService.abortController?.abort();
  }, []);
  React.useEffect(() => {
    let ev = BackHandler.addEventListener('hardwareBackPress', cb);

    return () => {
      ev.remove();
    };
  }, [cb]);

  return (
    <NavigationContainer ref={ref} theme={theme}>
      <RootStackScreen />
    </NavigationContainer>
  );
}
