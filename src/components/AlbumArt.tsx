import React from 'react';

import {
  View,
  StyleSheet,
  Image,
  // TouchableOpacity,
  Dimensions,
} from 'react-native';

type AlbumArtProps = {
  url: string;
  onPress?: () => void;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const AlbumArt = ({url, onPress}: AlbumArtProps) => (
  <View style={styles.container}>
    {/* <TouchableOpacity onPress={onPress}> */}
    <Image style={styles.image} source={{uri: url}} />
    {/* </TouchableOpacity> */}
  </View>
);

export default AlbumArt;

const {width} = Dimensions.get('window');
const imageSize = width - 48;

const styles = StyleSheet.create({
  container: {
    paddingLeft: 24,
    paddingRight: 24,
  },
  image: {
    width: imageSize,
    height: imageSize,
  },
});
