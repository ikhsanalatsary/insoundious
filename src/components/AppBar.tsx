import React from 'react';
import {Appbar} from 'react-native-paper';
import {SafeAreaLayout, SaveAreaInset} from './SafeAreaLayout';
import {StyleProp, ViewStyle} from 'react-native';
import {Theme} from 'react-native-paper/lib/typescript/src/types';

interface AppbarProps {
  children: React.ReactNode;
  style?: StyleProp<ViewStyle>;
  containerStyle?: StyleProp<ViewStyle>;
  dark?: boolean;
  theme?: Theme;
}

interface AppBarHeaderProps extends AppbarProps {
  /**
   * Extra padding to add at the top of header to account for translucent status bar.
   * This is automatically handled on iOS >= 11 including iPhone X using `SafeAreaView`.
   * If you are using Expo, we assume translucent status bar and set a height for status bar automatically.
   * Pass `0` or a custom value to disable the default behaviour, and customize the height.
   */
  statusBarHeight?: number;
}

export function AppBar(props: AppbarProps) {
  return (
    <SafeAreaLayout insets={SaveAreaInset.TOP} style={props.containerStyle}>
      <Appbar style={props.style} theme={props.theme} dark={props.dark}>
        {props.children}
      </Appbar>
    </SafeAreaLayout>
  );
}

export function AppBarHeader(props: AppBarHeaderProps) {
  return (
    <SafeAreaLayout insets={SaveAreaInset.TOP} style={props.containerStyle}>
      <Appbar.Header
        style={props.style}
        theme={props.theme}
        dark={props.dark}
        statusBarHeight={props.statusBarHeight}>
        {props.children}
      </Appbar.Header>
    </SafeAreaLayout>
  );
}
