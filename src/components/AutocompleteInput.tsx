/** Re-implement from react-native-autocomplete-input */
import React, {Component, ReactNode} from 'react';
import PropTypes from 'prop-types';
import {
  FlatList,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
  ViewPropTypes as RNViewPropTypes,
  TextInputProperties,
  StyleProp,
  ViewStyle,
  FlatListProps,
  GestureResponderHandlers,
  // ListViewProperties,
  ListRenderItem,
  NativeSyntheticEvent,
  TextInputEndEditingEventData,
} from 'react-native';

// Keep this line for downwards compatibility with RN.
//@ts-ignore
const ViewPropTypes = RNViewPropTypes || View.propTypes;

export interface AutocompleteProps<T> extends TextInputProperties {
  /**
   * style
   * These styles will be applied to the container which surrounds the autocomplete component.
   */
  containerStyle?: StyleProp<ViewStyle>;

  /**
   * bool
   * Set to true to hide the suggestion list.
   */
  hideResults?: boolean;

  /**
   * array
   * An array with suggestion items to be rendered in renderItem({ item, index }). Any array with length > 0 will open the suggestion list and any array with length < 1 will hide the list.
   */
  data: T[];

  /**
   * object
   * Props to pass on to the underlying FlatList.
   */
  flatListProps?: Partial<FlatListProps<T>>;

  /**
   * style
   * These styles will be applied to the container which surrounds the textInput component.
   */
  inputContainerStyle?: StyleProp<ViewStyle>;

  /**
   * function
   * keyExtractor will be called to get key for each item. It's up to you which string to return as a key.
   */
  keyExtractor?(item: T, i: number): string;

  /**
   * style
   * These styles will be applied to the container which surrounds the result list.
   */
  listContainerStyle?: StyleProp<ViewStyle>;

  /**
   * style
   * These style will be applied to the result list.
   */
  listStyle?: StyleProp<ViewStyle>;

  /**
   * function
   * onShowResult will be called when the autocomplete suggestions appear or disappear.
   */
  onShowResults?(showResults: boolean): void;

  /**
   * function
   * onStartShouldSetResponderCapture will be passed to the result list view container (onStartShouldSetResponderCapture).
   */
  onStartShouldSetResponderCapture?: GestureResponderHandlers['onStartShouldSetResponderCapture'];

  /**
   * function
   * renderItem will be called to render the data objects which will be displayed in the result view below the text input.
   */
  renderItem: ListRenderItem<T>;

  /**
   * function
   * render custom TextInput. All props passed to this function.
   */
  renderTextInput?(props: TextInputProperties): ReactNode;

  /**
   * Called once when the scroll position gets within onEndReachedThreshold of the rendered content.
   */
  onEndReached?: ((info: {distanceFromEnd: number}) => void) | null;

  /**
   * How far from the end (in units of visible length of the list) the bottom edge of the
   * list must be from the end of the content to trigger the `onEndReached` callback.
   * Thus a value of 0.5 will trigger `onEndReached` when the end of the content is
   * within half the visible length of the list.
   */
  onEndReachedThreshold?: number | null;

  /**
   * Determines when the keyboard should stay visible after a tap.
   * - 'never' (the default), tapping outside of the focused text input when the keyboard is up dismisses the keyboard. When this happens, children won't receive the tap.
   * - 'always', the keyboard will not dismiss automatically, and the scroll view will not catch taps, but children of the scroll view can catch taps.
   * - 'handled', the keyboard will not dismiss automatically when the tap was handled by a children, (or captured by an ancestor).
   * - false, deprecated, use 'never' instead
   * - true, deprecated, use 'always' instead
   */
  keyboardShouldPersistTaps?: boolean | 'always' | 'never' | 'handled';
}

export default class Autocomplete<T> extends Component<AutocompleteProps<T>> {
  static propTypes = {
    //@ts-ignore
    ...TextInput.propTypes,
    /**
     * These styles will be applied to the container which
     * surrounds the autocomplete component.
     */
    containerStyle: ViewPropTypes.style,
    /**
     * Assign an array of data objects which should be
     * rendered in respect to the entered text.
     */
    data: PropTypes.array,
    /**
     * Set to `true` to hide the suggestion list.
     */
    hideResults: PropTypes.bool,
    /*
     * These styles will be applied to the container which surrounds
     * the textInput component.
     */
    inputContainerStyle: ViewPropTypes.style,
    /*
     * Set `keyboardShouldPersistTaps` to true if RN version is <= 0.39.
     */
    keyboardShouldPersistTaps: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.bool,
    ]),
    /*
     * These styles will be applied to the container which surrounds
     * the result list.
     */
    listContainerStyle: ViewPropTypes.style,
    /**
     * These style will be applied to the result list.
     */
    listStyle: ViewPropTypes.style,
    /**
     * `onShowResults` will be called when list is going to
     * show/hide results.
     */
    onShowResults: PropTypes.func,
    /**
     * method for intercepting swipe on ListView. Used for ScrollView support on Android
     */
    onStartShouldSetResponderCapture: PropTypes.func,
    /**
     * `renderItem` will be called to render the data objects
     * which will be displayed in the result view below the
     * text input.
     */
    renderItem: PropTypes.func,
    keyExtractor: PropTypes.func,
    /**
     * renders custom TextInput. All props passed to this function.
     */
    renderTextInput: PropTypes.func,
    flatListProps: PropTypes.object,
  };

  static defaultProps = {
    data: [],
    keyboardShouldPersistTaps: 'always',
    onStartShouldSetResponderCapture: () => false,
    //@ts-ignore
    renderItem: ({item}) => <Text>{item}</Text>,
    renderTextInput: (
      props: JSX.IntrinsicAttributes &
        JSX.IntrinsicClassAttributes<TextInput> &
        Readonly<import('react-native').TextInputProps> &
        Readonly<{children?: React.ReactNode}>,
    ) => <TextInput {...props} />,
    flatListProps: {},
  };
  resultList: FlatList<T> | null = null;
  textInput: TextInput | null = null;

  constructor(props: AutocompleteProps<T>) {
    super(props);

    this.onRefListView = this.onRefListView.bind(this);
    this.onRefTextInput = this.onRefTextInput.bind(this);
    this.onEndEditing = this.onEndEditing.bind(this);
  }

  onEndEditing(e: NativeSyntheticEvent<TextInputEndEditingEventData>) {
    this.props.onEndEditing && this.props.onEndEditing(e);
  }

  onRefListView(resultList: FlatList<T> | null) {
    this.resultList = resultList;
  }

  onRefTextInput(textInput: TextInput | null) {
    this.textInput = textInput;
  }

  /**
   * Proxy `blur()` to autocomplete's text input.
   */
  blur() {
    const {textInput} = this;
    textInput && textInput.blur();
  }

  /**
   * Proxy `focus()` to autocomplete's text input.
   */
  focus() {
    const {textInput} = this;
    textInput && textInput.focus();
  }

  /**
   * Proxy `isFocused()` to autocomplete's text input.
   */
  isFocused() {
    const {textInput} = this;
    return textInput && textInput.isFocused();
  }

  renderResultList() {
    const {
      data,
      listStyle,
      renderItem,
      keyExtractor,
      keyboardShouldPersistTaps,
      flatListProps,
      onEndReached,
      onEndReachedThreshold,
    } = this.props;

    return (
      <FlatList
        ref={this.onRefListView}
        data={data}
        keyboardShouldPersistTaps={keyboardShouldPersistTaps}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        onEndReached={onEndReached}
        onEndReachedThreshold={onEndReachedThreshold}
        style={[styles.list, listStyle]}
        {...flatListProps}
      />
    );
  }

  renderTextInput() {
    const {renderTextInput, style} = this.props;
    const props = {
      style: [styles.input, style],
      ref: this.onRefTextInput,
      onEndEditing: this.onEndEditing,
      ...this.props,
    };

    //@ts-ignore
    return renderTextInput(props);
  }

  render() {
    const {
      data,
      // containerStyle,
      hideResults,
      // inputContainerStyle,
      listContainerStyle,
      onShowResults,
      onStartShouldSetResponderCapture,
    } = this.props;
    const showResults = data.length > 0;

    // Notify listener if the suggestion will be shown.
    onShowResults && onShowResults(showResults);

    // return (
    //   <View style={[styles.container, containerStyle]}>
    //     <View style={[styles.inputContainer, inputContainerStyle]}>
    //       {this.renderTextInput()}
    //     </View>
    //     {!hideResults && (
    //       <View
    //         style={listContainerStyle}
    //         onStartShouldSetResponderCapture={onStartShouldSetResponderCapture}
    //       >
    //         {showResults && this.renderResultList()}
    //       </View>
    //     )}
    //   </View>
    // );

    return (
      <>
        {this.renderTextInput()}
        {!hideResults && (
          <View
            style={listContainerStyle}
            onStartShouldSetResponderCapture={onStartShouldSetResponderCapture}>
            {showResults && this.renderResultList()}
          </View>
        )}
      </>
    );
  }
}

const border = {
  borderColor: '#b9b9b9',
  borderRadius: 1,
  borderWidth: 1,
};

const androidStyles = {
  container: {
    flex: 1,
  },
  inputContainer: {
    ...border,
    marginBottom: 0,
  },
  list: {
    ...border,
    backgroundColor: 'white',
    borderTopWidth: 0,
    margin: 10,
    marginTop: 0,
  },
};

const iosStyles = {
  container: {
    zIndex: 1,
  },
  inputContainer: {
    ...border,
  },
  input: {
    backgroundColor: 'white',
    height: 40,
    paddingLeft: 3,
  },
  list: {
    ...border,
    backgroundColor: 'white',
    borderTopWidth: 0,
    left: 0,
    position: 'absolute',
    right: 0,
  },
};

//@ts-ignore
const styles = StyleSheet.create({
  input: {
    backgroundColor: 'white',
    height: 40,
    paddingLeft: 3,
  },
  ...Platform.select({
    android: {...androidStyles},
    ios: {...iosStyles},
  }),
});
