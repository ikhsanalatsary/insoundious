import React from 'react';

import {View, StyleSheet, StyleProp, ViewStyle} from 'react-native';
import {
  IconButton,
  Text,
  withTheme,
  Theme,
  TouchableRipple,
  Avatar,
} from 'react-native-paper';
import ProgressBar from './ProgressBar';
import TrackPlayer, {State} from 'react-native-track-player';
import {truncateString, playOrPause} from 'src/utils';
import {Layout} from './Layout';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootStackParamList} from 'src/Router';
import {useNavigation} from '@react-navigation/native';

type NavigationProp = StackNavigationProp<RootStackParamList, 'Main'>;
// type HomeRouteProp = RouteProp<RootStackParamList, 'Main'>;

type Props = {
  theme: Theme;
  style?: StyleProp<ViewStyle>;
  trackTitle: string;
  trackArtist: string;
  trackArtwork: string;
  playbackState: State;
};

const ControlBar = ({
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  theme,
  style,
  trackTitle,
  trackArtist,
  trackArtwork,
  playbackState,
}: Props) => {
  const navigation = useNavigation();
  var iconName = 'play';
  if (
    playbackState === TrackPlayer.STATE_PLAYING ||
    playbackState === TrackPlayer.STATE_BUFFERING
  ) {
    iconName = 'pause';
  }

  return (
    // @ts-ignore
    <Layout style={[styles.bottom, style]}>
      <ProgressBar />
      <View style={styles.container}>
        <Avatar.Image size={30} source={{uri: trackArtwork}} />
        {/* <Image source={{uri: trackArtwork}} style={{width: 10}} /> */}
        {/* <IconButton
          icon="playlist-plus"
          // color={Colors.red500}
          size={20}
          onPress={() => console.log('Pressed')}
        /> */}
        {/* <Text numberOfLines={1} ellipsizeMode="tail">
          {trackTitle}
        </Text>
        <Text numberOfLines={1} ellipsizeMode="tail">
          {trackArtist}
        </Text> */}
        <TouchableRipple
          onPress={() =>
            navigation.navigate('Player', {playerState: playbackState})
          }>
          <View style={styles.column}>
            <Text style={styles.title}>{truncateString(trackTitle, 30)}</Text>
            <Text style={styles.artist}>{truncateString(trackArtist, 25)}</Text>
          </View>
        </TouchableRipple>

        <IconButton
          icon={iconName}
          // color={Colors.red500}
          size={20}
          onPress={() => {
            playOrPause();
          }}
        />
      </View>
    </Layout>
  );
};

export default withTheme(ControlBar);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // paddingTop: 8,
  },
  bottom: {
    height: 50,
    // alignItems: 'center',
    paddingHorizontal: 4,
    // elevation: 4,
    // flexDirection: 'column',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 50,
    zIndex: 1,
  },
  title: {
    fontSize: 12,
  },
  artist: {
    fontSize: 8,
  },
  column: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
});
