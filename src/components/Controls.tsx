/* eslint-disable react-native/no-inline-styles */
import React from 'react';

import {View, StyleSheet, TouchableOpacity} from 'react-native';
// import Icon from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Colors, withTheme} from 'react-native-paper';
import {Theme} from 'react-native-paper/lib/typescript/src/types';

type ControlProps = {
  paused: boolean;
  shuffleOn: boolean;
  repeatOn: boolean;
  onTogglePlayback: () => void;
  onPrevious: () => void;
  onForward: () => void;
  onPressShuffle: () => void;
  onPressRepeat: () => void;
  forwardDisabled: boolean;
  theme: Theme;
};

const Controls = withTheme(
  ({
    paused,
    shuffleOn,
    repeatOn,
    onTogglePlayback,
    onPrevious,
    onForward,
    onPressShuffle,
    onPressRepeat,
    forwardDisabled,
    theme,
  }: ControlProps) => {
    let color = theme.dark ? Colors.white : Colors.black;
    return (
      <View style={styles.container}>
        <TouchableOpacity activeOpacity={0.0} onPress={onPressShuffle}>
          <Icon
            style={shuffleOn ? null : styles.off}
            name="shuffle"
            color={color}
            size={24}
          />
        </TouchableOpacity>
        <View style={{width: 40}} />
        <TouchableOpacity onPress={onPrevious}>
          <Icon name="skip-previous" size={36} color={color} />
        </TouchableOpacity>
        <View style={{width: 20}} />
        <TouchableOpacity onPress={onTogglePlayback}>
          <View style={[styles.playButton, {borderColor: color}]}>
            <Icon
              style={{alignSelf: 'center', margin: 0}}
              name={!paused ? 'pause' : 'play'}
              color={color}
              size={48}
            />
          </View>
        </TouchableOpacity>

        <View style={{width: 20}} />
        <TouchableOpacity onPress={onForward} disabled={forwardDisabled}>
          <Icon
            style={[forwardDisabled && {opacity: 0.3}]}
            name="skip-next"
            size={36}
            color={color}
          />
        </TouchableOpacity>
        <View style={{width: 40}} />
        <TouchableOpacity activeOpacity={0.0} onPress={onPressRepeat}>
          <Icon
            style={repeatOn ? null : styles.off}
            name="repeat"
            color={color}
            size={24}
          />
        </TouchableOpacity>
      </View>
    );
  },
);

export default Controls;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 8,
  },
  noGap: {
    margin: 0,
  },
  playButton: {
    height: 72,
    width: 72,
    borderWidth: 1,
    // borderColor: 'white',
    borderRadius: 72 / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  secondaryControl: {
    height: 18,
    width: 18,
  },
  off: {
    opacity: 0.3,
  },
});
