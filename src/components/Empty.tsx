import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {Text, Button} from 'react-native-paper';
import {Layout} from './Layout';

export default function Empty(props: {onPress: () => void}) {
  return (
    <Layout style={styles.container}>
      <View style={styles.align}>
        <Text>Alfred says something is not right</Text>
        <Button
          icon="refresh"
          mode="contained"
          style={styles.refresh}
          onPress={props.onPress}>
          Refresh
        </Button>
      </View>
    </Layout>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  refresh: {
    margin: 8,
  },
  align: {alignItems: 'center'},
});
