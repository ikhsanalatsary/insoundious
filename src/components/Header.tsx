import React from 'react';

import {View, StyleSheet} from 'react-native';
import {IconButton, Text, Colors, withTheme} from 'react-native-paper';
import {Theme} from 'react-native-paper/lib/typescript/src/types';

const Header = withTheme(
  (props: {
    message: string;
    onDownPress?: () => void;
    onQueuePress?: () => void;
    onMessagePress?: () => void;
    theme: Theme;
  }) => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const {message, onDownPress, onQueuePress, onMessagePress, theme} = props;
    const color = theme.dark ? Colors.white : Colors.black;
    return (
      <View style={styles.container}>
        <IconButton
          icon="chevron-down"
          size={20}
          onPress={onDownPress}
          color={color}
          // eslint-disable-next-line react-native/no-inline-styles
          style={{margin: 0}}
          animated
        />
        <Text onPress={onMessagePress} style={[styles.message, {color}]}>
          {message.toUpperCase()}
        </Text>
        <View />
        {/* <TouchableOpacity onPress={onQueuePress}>
        <Image
          style={styles.button}
          source={require('../img/ic_queue_music_white.png')}
        />
      </TouchableOpacity> */}
      </View>
    );
  },
);

export default Header;

const styles = StyleSheet.create({
  container: {
    height: 72,
    paddingTop: 20,
    paddingLeft: 12,
    paddingRight: 12,
    flexDirection: 'row',
  },
  message: {
    flex: 1,
    textAlign: 'center',
    color: Colors.white,
    fontWeight: 'bold',
    fontSize: 10,
    marginTop: 10,
  },
  button: {
    opacity: 0.72,
  },
});
