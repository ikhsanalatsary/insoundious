import React from 'react';
import {View, ViewProps} from 'react-native';
import {withTheme, Colors, overlay} from 'react-native-paper';
import {elevation} from 'src/themes';
import {Theme} from 'react-native-paper/lib/typescript/src/types';

interface Props extends LayoutProps {
  theme: Theme;
}

export type LayoutElement = React.ReactElement<LayoutProps>;
export interface LayoutProps extends ViewProps {
  children?: React.ReactNode;
}

export function LayoutComponent(props: Props) {
  const {style, theme, ...derivedProps} = props;
  let backgroundColor = theme.dark
    ? overlay(elevation, theme.colors.surface)
    : Colors.white;

  // TODO: fix this
  // @ts-ignore
  return <View {...derivedProps} style={[{backgroundColor}, style]} />;
}

export const Layout = withTheme<Props, typeof LayoutComponent>(LayoutComponent);
