import React from 'react';
import {View, FlatList} from 'react-native';
import {Card, Caption, ActivityIndicator} from 'react-native-paper';
import {ITrack} from 'src/domains/Track';
import {placeholderImage} from 'src/themes';

export default React.memo(
  function Search({
    results,
    loading,
    onPress,
  }: {
    results: ITrack[];
    loading: boolean;
    onPress?: () => void;
  }) {
    return (
      <FlatList
        data={results}
        // extraData={results.length > 0}
        keyExtractor={item => item.videoId}
        ListEmptyComponent={() => (
          <View style={{alignItems: 'center'}}>
            <Caption>Search</Caption>
          </View>
        )}
        ListFooterComponent={() =>
          loading ? <ActivityIndicator size="large" animating={true} /> : null
        }
        renderItem={({item, index}) => {
          let thumbnail = item?.videoThumbnails?.find(
            v => v.quality === 'medium',
          );
          return (
            <Card onPress={onPress}>
              <Card.Cover source={{uri: thumbnail?.url ?? placeholderImage}} />
              <Card.Content>
                <Caption>{item.title}</Caption>
              </Card.Content>
            </Card>
          );
        }}
      />
    );
  },
  (prevProps, nextProps) => {
    return prevProps.results === nextProps.results;
  },
);
