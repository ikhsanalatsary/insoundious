import * as React from 'react';
import {StyleSheet} from 'react-native';
import {ActivityIndicator} from 'react-native-paper';
import {Layout} from './Layout';

export default function Loading() {
  return (
    <Layout style={styles.container}>
      <ActivityIndicator size="large" animating={true} />
    </Layout>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});
