import React, {useState, useEffect, useCallback} from 'react';
import {StyleProp, ViewStyle} from 'react-native';
// import {
//   BottomNavigation,
//   BottomNavigationTab,
//   Divider,
//   BottomNavigationTabElement,
// } from '@ui-kitten/components';
import {
  useNavigationBuilder,
  createNavigatorFactory,
  DefaultNavigatorOptions,
} from '@react-navigation/native';
import {BottomTabView} from '@react-navigation/bottom-tabs';
import {
  TabRouter,
  // TabActions,
  TabRouterOptions,
  // TabNavigationState,
} from '@react-navigation/routers';
import TrackPlayer from 'react-native-track-player';
import {
  // SafeAreaLayout,
  SafeAreaLayoutElement,
  // SaveAreaInset,
} from './SafeAreaLayout';
import ControlBar from './ControlBar';
import {placeholderImage} from 'src/themes';

const {
  // usePlaybackState,
  useTrackPlayerEvents,
  TrackPlayerEvents,
} = TrackPlayer;

// Props accepted by the view
type TabNavigationConfig = {
  tabBarStyle: StyleProp<ViewStyle>;
  contentStyle: StyleProp<ViewStyle>;
};

// Supported screen options
type TabNavigationOptions = {
  title?: string;
};

// Map of events and the type of data (in event.data)
type TabNavigationEventMap = {
  tabPress: {isAlreadyFocused: boolean};
};

// The props accepted by the component is a combination of 3 things
type Props = DefaultNavigatorOptions<TabNavigationOptions> &
  TabRouterOptions &
  TabNavigationConfig & {[key: string]: any};

function MyBottomTabBar({
  initialRouteName,
  backBehavior,
  children,
  screenOptions,
  ...rest
}: Props): SafeAreaLayoutElement {
  const {state, descriptors, navigation} = useNavigationBuilder(TabRouter, {
    initialRouteName,
    backBehavior,
    children,
    screenOptions,
  });

  const [playbackState, setState] = useState(TrackPlayer.STATE_NONE);
  const [trackTitle, setTrackTitle] = useState('');
  const [trackArtist, setTrackArtist] = useState('');
  const [trackImage, setTrackImage] = useState(placeholderImage);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [showControl, setShowControl] = useState(false);
  console.log('playback ', playbackState);
  useEffect(
    useCallback(() => {
      getTrack().then(track => {
        if (track) {
          setTrackTitle(track.title);
          setTrackArtist(track.artist);
          setTrackImage(track.artwork || placeholderImage);
          setState(track.state);
        }
      });
    }, []),
  );
  useTrackPlayerEvents(
    [
      TrackPlayerEvents.PLAYBACK_TRACK_CHANGED,
      TrackPlayerEvents.PLAYBACK_STATE,
    ],
    async event => {
      // TODO: check here
      console.log('event.type ', event.type);
      console.log('event.nextTrack ', event.nextTrack);
      console.log('event.state ', event.state);
      switch (event.type) {
        case TrackPlayer.TrackPlayerEvents.PLAYBACK_TRACK_CHANGED:
          const track = await TrackPlayer.getTrack(event.nextTrack);
          if (track) {
            setTrackTitle(track.title);
            setTrackArtist(track.artist);
            setTrackImage(track.artwork || placeholderImage);
          }
          break;
        case TrackPlayerEvents.PLAYBACK_STATE:
          setState(event.state);
          break;
        default:
          break;
      }
      if (
        !event.nextTrack &&
        (playbackState === TrackPlayer.STATE_PLAYING ||
          playbackState === TrackPlayer.STATE_BUFFERING)
      ) {
        let trackId = await TrackPlayer.getCurrentTrack();
        const track = await TrackPlayer.getTrack(trackId);
        if (track) {
          setTrackTitle(track.title);
          setTrackArtist(track.artist);
          setTrackImage(track.artwork || placeholderImage);
        }
      }

      // if (
      //   playbackState === TrackPlayer.STATE_PLAYING ||
      //   playbackState === TrackPlayer.STATE_BUFFERING ||
      //   playbackState === TrackPlayer.STATE_PAUSED
      // ) {
      //   setShowControl(true);
      // } else {
      //   setShowControl(false);
      // }
    },
  );
  // TODO: find away to show or hide control bar
  console.log(showControl);
  return (
    <>
      <ControlBar
        playbackState={playbackState}
        trackTitle={trackTitle}
        trackArtist={trackArtist}
        trackArtwork={trackImage}
      />
      <BottomTabView
        {...rest}
        state={state}
        navigation={navigation}
        descriptors={descriptors}
      />
    </>
  );
  // return (
  //   <BottomTabView
  //     {...rest}
  //     state={state}
  //     navigation={navigation}
  //     descriptors={descriptors}
  //   />
  // );
}

async function getTrack() {
  try {
    let id = await TrackPlayer.getCurrentTrack();
    let track = await TrackPlayer.getTrack(id);
    let state = await TrackPlayer.getState();
    if (track) {
      return {...track, state};
    }

    return undefined;
  } catch (error) {
    return undefined;
  }
}

export default createNavigatorFactory(MyBottomTabBar);

// export const MyBottomTabBar = (props): SafeAreaLayoutElement => {
//   const onSelect = (index: number): void => {
//     const selectedTabRoute: string = props.state.routeNames[index];
//     props.navigation.navigate(selectedTabRoute);
//   };

//   const createNavigationTabForRoute = (route): BottomNavigationTabElement => {
//     const {options} = props.descriptors[route.key];
//     return (
//       <BottomNavigationTab
//         key={route.key}
//         title={options.title}
//         icon={options.tabBarIcon}
//       />
//     );
//   };

//   return (
//     <SafeAreaLayout insets={SaveAreaInset.BOTTOM}>
//       <Divider />
//       <BottomNavigation
//         appearance="noIndicator"
//         selectedIndex={props.state.index}
//         onSelect={onSelect}>
//         {props.state.routes.map(createNavigationTabForRoute)}
//       </BottomNavigation>
//     </SafeAreaLayout>
//   );
// };
