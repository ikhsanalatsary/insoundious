import React, {useState} from 'react';
import TrackPlayer from 'react-native-track-player';
import {StyleSheet, View} from 'react-native';
import Controls from './Controls';
import AlbumArt from './AlbumArt';
import TrackDetails from './TrackDetail';
import SeekBar from './SeekBar';
import {Colors} from 'react-native-paper';
import {placeholderImage} from 'src/themes';

const {
  // useTrackPlayerProgress,
  usePlaybackState,
  useTrackPlayerEvents,
} = TrackPlayer;

// function ControlButton({title, onPress}: {title: string; onPress: () => void}) {
//   return (
//     <TouchableOpacity style={styles.controlButtonContainer} onPress={onPress}>
//       <Text style={styles.controlButtonText}>{title}</Text>
//     </TouchableOpacity>
//   );
// }

export default function Player(props: {
  style: object;
  onNext: () => Promise<void>;
  onPrevious: () => Promise<void>;
  onTogglePlayback: () => Promise<void>;
  stateName: string;
}) {
  const playbackState = usePlaybackState();
  const [trackTitle, setTrackTitle] = useState('');
  const [trackArtwork, setTrackArtwork] = useState('');
  const [trackArtist, setTrackArtist] = useState('');
  useTrackPlayerEvents(['playback-track-changed'], async event => {
    if (event.type === TrackPlayer.TrackPlayerEvents.PLAYBACK_TRACK_CHANGED) {
      const track = await TrackPlayer.getTrack(event.nextTrack);
      setTrackTitle(track.title);
      setTrackArtist(track.artist);
      setTrackArtwork(track.artwork);
    }
  });

  const {style, onNext, onPrevious, onTogglePlayback /*stateName*/} = props;

  // var middleButtonText = 'Play';
  var paused = true;

  if (
    playbackState === TrackPlayer.STATE_PLAYING ||
    playbackState === TrackPlayer.STATE_BUFFERING
  ) {
    // middleButtonText = 'Pause';
    paused = false;
  }

  return (
    <View style={[styles.container, style]}>
      <AlbumArt url={trackArtwork || placeholderImage} />
      <TrackDetails title={trackTitle} artist={trackArtist} />
      <SeekBar onSeek={seekTo} />
      <Controls
        onPressRepeat={() => {}}
        repeatOn={false}
        shuffleOn={false}
        forwardDisabled={false}
        onPressShuffle={() => {}}
        onTogglePlayback={onTogglePlayback}
        onPrevious={onPrevious}
        onForward={onNext}
        paused={paused}
      />
    </View>
  );
}

Player.defaultProps = {
  style: {},
};

async function seekTo(seconds: number) {
  try {
    await TrackPlayer.seekTo(seconds);
  } catch (_) {}
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // backgroundColor: 'rgb(4,4,4)',
  },
  audioElement: {
    height: 0,
    width: 0,
  },
  progress: {
    height: 1,
    width: '90%',
    marginTop: 10,
    flexDirection: 'row',
  },
  title: {
    marginTop: 10,
  },
  artist: {
    fontWeight: 'bold',
  },
  controls: {
    marginVertical: 20,
    flexDirection: 'row',
  },
  controlButtonContainer: {
    flex: 1,
  },
  controlButtonText: {
    fontSize: 18,
    textAlign: 'center',
  },
  state: {
    marginVertical: 2,
    color: Colors.white,
    alignSelf: 'center',
  },
});
