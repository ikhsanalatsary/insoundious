/// <reference path='src/src/typings/Slider.d.ts' />
import React from 'react';
import {StyleSheet} from 'react-native';
// import {ProgressBar as Progressbar} from 'react-native-paper';
import TrackPlayer from 'react-native-track-player';
import Slider from './Slider';

const {useTrackPlayerProgress} = TrackPlayer;

export default function ProgressBar() {
  const progress = useTrackPlayerProgress();

  return (
    <Slider
      disabled
      maximumValue={Math.max(progress.duration, 1, progress.position + 1)}
      value={progress.position}
      thumbTouchSize={{height: 0, width: 0}}
      style={styles.slider}
      thumbStyle={styles.thumb}
      trackStyle={styles.track}
    />
  );
}

const styles = StyleSheet.create({
  slider: {
    margin: -20,
  },
  track: {
    height: 2,
    borderRadius: 1,
  },
  thumb: {
    width: 0,
    height: 0,
    // borderRadius: 5,
    // backgroundColor: 'white',
  },
});
