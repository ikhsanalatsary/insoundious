/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import Slider from './Slider';
import {useTrackPlayerProgress} from 'react-native-track-player';
import {minutesAndSeconds} from 'src/utils';
import {withTheme, Colors, Text} from 'react-native-paper';
import {Theme} from 'react-native-paper/lib/typescript/src/types';

type SeekBarProps = {
  onSeek: (seconds: number) => void;
  onSlidingStart?: (val: number) => void;
  theme: Theme;
};

const SeekBar = ({
  onSeek,
  onSlidingStart = (_val: number) => {},
  theme,
}: SeekBarProps) => {
  const progress = useTrackPlayerProgress();
  const elapsed = minutesAndSeconds(progress.position);
  const remaining = minutesAndSeconds(progress.duration - progress.position);
  const [value, setValue] = useState(0);
  const color = theme.dark ? Colors.white : Colors.black;
  //   console.log(Math.floor(value));
  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <Text style={styles.text}>{elapsed[0] + ':' + elapsed[1]}</Text>
        <View style={{flex: 1}} />
        <Text style={[styles.text, {width: 40}]}>
          {progress.duration > 1 && '-' + remaining[0] + ':' + remaining[1]}
        </Text>
      </View>
      <Slider
        maximumValue={Math.max(progress.duration, 1, progress.position + 1)}
        onSlidingStart={onSlidingStart}
        onSlidingComplete={() => onSeek(value)}
        value={progress.position}
        style={styles.slider}
        minimumTrackTintColor={color}
        maximumTrackTintColor={
          theme.dark ? 'rgba(255, 255, 255, 0.14)' : Colors.grey300
        }
        thumbStyle={[styles.thumb, {backgroundColor: color}]}
        trackStyle={styles.track}
        onValueChange={(val: number) => setValue(Math.floor(val))}
      />
    </View>
  );
};

export default withTheme(SeekBar);

const styles = StyleSheet.create({
  slider: {
    marginTop: -12,
  },
  container: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 16,
  },
  track: {
    height: 2,
    borderRadius: 1,
  },
  thumb: {
    width: 10,
    height: 10,
    borderRadius: 5,
    // backgroundColor: 'white',
  },
  text: {
    // color: 'rgba(255, 255, 255, 0.72)',
    fontSize: 12,
    textAlign: 'center',
  },
});
