import React from 'react';

import {View, StyleSheet} from 'react-native';
import {withTheme, Text, Colors} from 'react-native-paper';
import {Theme} from 'react-native-paper/lib/typescript/src/types';

type TrackDetailsProps = {
  title: string;
  artist: string;
  onAddPress?: () => void;
  onMorePress?: () => void;
  onTitlePress?: () => void;
  onArtistPress?: () => void;
  theme: Theme;
};

const TrackDetails = ({
  title,
  artist,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onAddPress,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onMorePress,
  onTitlePress,
  onArtistPress,
  theme,
}: TrackDetailsProps) => (
  <View style={styles.container}>
    {/* <TouchableOpacity onPress={onAddPress}>
      <Image
        style={styles.button}
        source={require('../img/ic_add_circle_outline_white.png')}
      />
    </TouchableOpacity> */}
    <View style={styles.detailsWrapper}>
      <Text style={styles.title} onPress={onTitlePress}>
        {title}
      </Text>
      <Text
        style={[styles.artist, !theme.dark && {color: Colors.grey700}]}
        onPress={onArtistPress}>
        {artist}
      </Text>
    </View>
    {/* <TouchableOpacity onPress={onMorePress}>
      <View style={styles.moreButton}>
        <Image
          style={styles.moreButtonIcon}
          source={require('../img/ic_more_horiz_white.png')}
        />
      </View>
    </TouchableOpacity> */}
  </View>
);

export default withTheme(TrackDetails);

const styles = StyleSheet.create({
  container: {
    paddingTop: 24,
    flexDirection: 'row',
    paddingLeft: 20,
    alignItems: 'center',
    paddingRight: 20,
  },
  detailsWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  artist: {
    color: 'rgba(255, 255, 255, 0.72)',
    fontSize: 12,
    marginTop: 4,
  },
  button: {
    opacity: 0.72,
  },
  moreButton: {
    borderColor: 'rgb(255, 255, 255)',
    borderWidth: 2,
    opacity: 0.72,
    borderRadius: 10,
    width: 20,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  moreButtonIcon: {
    height: 17,
    width: 17,
  },
});
