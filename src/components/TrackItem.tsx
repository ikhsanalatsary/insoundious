import React from 'react';
import {Animated, StyleProp, StyleSheet} from 'react-native';
import {Card, Caption} from 'react-native-paper';
import Track from 'src/domains/Track';
import {placeholderImage} from 'src/themes';

const TrackItem = ({
  item,
  backgroundColor,
  onNavigate,
  horizontal = false,
}: {
  item: Track;
  backgroundColor: string | Animated.AnimatedInterpolation;
  onNavigate: () => void;
  horizontal?: boolean;
}) => {
  let thumbnail = item.videoThumbnails.find(v => v.quality === 'medium');
  return (
    <Card
      style={[
        horizontal && styles.card,
        {backgroundColor} as StyleProp<object>,
        {elevation: 0},
      ]}
      onPress={onNavigate}>
      <Card.Cover
        resizeMode="contain"
        resizeMethod="resize"
        style={horizontal && styles.cover}
        source={{uri: thumbnail?.url ?? placeholderImage}}
      />
      <Card.Content>
        <Caption>{item.title}</Caption>
      </Card.Content>
    </Card>
  );
};

export default TrackItem;

const styles = StyleSheet.create({
  card: {
    marginHorizontal: 8,
    height: 300,
    width: 200,
    flexWrap: 'wrap',
  },
  cover: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
  },
});
