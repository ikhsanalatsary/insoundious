/**
 * AbstractObject
 *
 * @abstract Class
 * @description to remove '_' prefix in enumerable property
 */
export interface IAbstractObject {
  toJSON<T>(): T;
}

export default abstract class AbstractObject {
  /**
   * @overide method
   * @description To remove _ from the field names
   */
  toJSON<T>() {
    let obj = {} as T;
    let self = (this as unknown) as T;
    for (let key of Object.keys(self)) {
      if (key.startsWith('_')) {
        obj[key.substring(1) as keyof T] = self[key as keyof T];
      } else {
        obj[key as keyof T] = self[key as keyof T];
      }
    }

    return obj;
  }
}
