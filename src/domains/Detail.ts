import {Thumbnail, Quality} from './Thumbnail';
import Domain, {IDomain} from './Domain';
import Track from './Track';

// Original TrackDetail
// export interface ITrackDetail {
//   type: string;
//   title: string;
//   videoId: string;
//   videoThumbnails: Thumbnail[];
//   storyboards: Storyboard[];
//   description: string;
//   descriptionHtml: string;
//   published: number;
//   publishedText: string;
//   keywords: string[];
//   viewCount: number;
//   likeCount: number;
//   dislikeCount: number;
//   paid: boolean;
//   premium: boolean;
//   isFamilyFriendly: boolean;
//   allowedRegions: string[];
//   genre: string;
//   genreUrl: string;
//   author: string;
//   authorId: string;
//   authorUrl: string;
//   authorThumbnails: Thumbnail[];
//   subCountText: string;
//   lengthSeconds: number;
//   allowRatings: boolean;
//   rating: number;
//   isListed: boolean;
//   liveNow: boolean;
//   isUpcoming: boolean;
//   dashUrl: string;
//   adaptiveFormats: AdaptiveFormat[];
//   formatStreams: FormatStream[];
//   captions: Caption[];
//   recommendedVideos: RecommendedVideo[];
// }

export interface IDetail extends IDomain {
  // storyboards: Storyboard[];
  // keywords: string[];
  // viewCount: number;
  likeCount: number;
  // dislikeCount: number;
  // isFamilyFriendly: boolean;
  // allowedRegions: string[];
  genre: string;
  genreUrl: string;
  // subCountText: string;
  // allowRatings: boolean;
  rating: number;
  // isListed: boolean;
  // liveNow: boolean;
  // isUpcoming: boolean;
  // dashUrl: string;
  adaptiveFormats: AdaptiveFormat[];
  formatStreams: FormatStream[];
  // captions: Caption[];
  recommendedVideos: NextTrack[];
}

export interface AdaptiveFormat {
  index: string;
  bitrate: string;
  init: string;
  url: string;
  itag: string;
  type: string;
  clen: string;
  lmt: string;
  projectionType: string;
  fps?: number;
  container?: Container;
  encoding?: string;
  resolution?: string;
  qualityLabel?: string;
}

export enum Container {
  M4A = 'm4a',
  Mp4 = 'mp4',
  Webm = 'webm',
}

export interface FormatStream {
  url: string;
  itag: string;
  type: string;
  quality: Quality;
  fps: number;
  container: Container;
  encoding: string;
  resolution: string;
  qualityLabel: string;
  size: string;
}

export interface RecommendedVideo {
  videoId: string;
  title: string;
  videoThumbnails: Thumbnail[];
  author: string;
  authorUrl: null;
  authorId: string;
  authorThumbnails: Thumbnail[];
  lengthSeconds: number;
  viewCountText: string;
  viewCount: number | null;
}

// export interface Storyboard {
//   url: string;
//   templateUrl: string;
//   width: number;
//   height: number;
//   count: number;
//   interval: number;
//   storyboardWidth: number;
//   storyboardHeight: number;
//   storyboardCount: number;
// }

// export interface Caption {
//   label: string;
//   languageCode: string;
//   url: string;
// }

export type NextTrack = Exclude<Track, keyof RecommendedVideo>;

export default abstract class Detail extends Domain implements IDetail {
  private _likeCount!: number;
  private _genre!: string;
  private _genreUrl!: string;
  private _rating!: number;
  private _adaptiveFormats: AdaptiveFormat[] = [];
  private _formatStreams: FormatStream[] = [];
  private _recommendedVideos: NextTrack[] = [];

  get likeCount() {
    return this._likeCount;
  }

  set likeCount(val: number) {
    this._likeCount = Number(val);
  }

  get genre() {
    return this._genre;
  }

  set genre(val: string) {
    this._genre = val;
  }

  get genreUrl() {
    return this._genreUrl;
  }

  set genreUrl(val: string) {
    this._genreUrl = val;
  }

  get rating() {
    return this._rating;
  }

  set rating(val: number) {
    this._rating = Number(val);
  }

  get adaptiveFormats() {
    return this._adaptiveFormats || [];
  }

  set adaptiveFormats(val: AdaptiveFormat[]) {
    this._adaptiveFormats = val;
  }

  get formatStreams() {
    return this._formatStreams || [];
  }

  set formatStreams(val: FormatStream[]) {
    this._formatStreams = val;
  }

  get recommendedVideos() {
    return this._recommendedVideos || [];
  }

  set recommendedVideos(val: NextTrack[]) {
    this._recommendedVideos = val;
  }
}
