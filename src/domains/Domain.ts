import AbstractObject, {IAbstractObject} from './AbstractObject';

export interface IDomain extends IAbstractObject {
  id: number;
  createdAt: string;
  updatedAt: string;
}

export default abstract class Domain extends AbstractObject {
  private _id!: number;
  private _createdAt!: string; // actually date
  private _updatedAt!: string; // actually date

  get id() {
    return this._id;
  }

  set id(val: number) {
    this._id = val;
  }

  get createdAt() {
    return this._createdAt;
  }

  set createdAt(val: string) {
    this._createdAt = val;
  }

  get updatedAt() {
    return this._updatedAt;
  }

  set updatedAt(val: string) {
    this._updatedAt = val;
  }
}
