import AbstractObject from './AbstractObject';

export interface ISetting {
  isDarkMode: boolean;
}

export default class Setting extends AbstractObject implements ISetting {
  private _isDarkMode: boolean = true;

  create(storage: ISetting) {
    this.isDarkMode = storage.isDarkMode;

    return this;
  }

  get isDarkMode() {
    return this._isDarkMode;
  }

  set isDarkMode(val: boolean) {
    this._isDarkMode = val;
  }
}
