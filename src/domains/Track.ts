import {Thumbnail} from './Thumbnail';
import Detail, {IDetail} from './Detail';

// To parse this data:
//
//   import { Convert, Trending } from "./file";
//
//   const trending = Convert.toTrending(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

export interface ITrack extends IDetail {
  type: string;
  title: string;
  videoId: string;
  author: string;
  authorId: string;
  authorUrl: string;
  authorThumbnails: Thumbnail[];
  videoThumbnails: Thumbnail[];
  description: string;
  // descriptionHtml: string;
  viewCount: number;
  // published: number;
  // publishedText: string;
  lengthSeconds: number;
  // liveNow: boolean;
  // paid: boolean;
  // premium: boolean;
}

export type MethodKeys = 'create' | 'extend' | 'toJSON';
export type OmitMethodTrack = Omit<Track, MethodKeys>;

export default class Track extends Detail implements ITrack {
  static serialize(track: Track): OmitMethodTrack {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const {create, extend, toJSON, ...rest} = track.toJSON();

    return rest as OmitMethodTrack;
  }

  private _type!: string;
  private _title!: string;
  private _videoId!: string;
  private _author!: string;
  private _authorId!: string;
  private _authorUrl!: string;
  private _authorThumbnails: Thumbnail[] = [];
  private _videoThumbnails: Thumbnail[] = [];
  private _description!: string;
  // private _descriptionHtml!: string;
  private _viewCount!: number;
  // private _published!: number;
  // private _publishedText!: string;
  private _lengthSeconds!: number;
  // private _liveNow!: boolean;
  // private _paid!: boolean;
  // private _premium!: boolean;

  get type() {
    return this._type;
  }

  set type(val: string) {
    this._type = val;
  }

  get title() {
    return this._title;
  }

  set title(val: string) {
    this._title = val;
  }

  get videoId() {
    return this._videoId;
  }

  set videoId(val: string) {
    this._videoId = val;
  }

  get author() {
    return this._author;
  }

  set author(val: string) {
    this._author = val;
  }

  get authorId() {
    return this._authorId;
  }

  set authorId(val: string) {
    this._authorId = val;
  }

  get authorUrl() {
    return this._authorUrl;
  }

  set authorUrl(val: string) {
    this._authorUrl = val;
  }

  get authorThumbnails() {
    return this._authorThumbnails || [];
  }

  set authorThumbnails(val: Thumbnail[]) {
    this._authorThumbnails = val;
  }

  get videoThumbnails() {
    return this._videoThumbnails || [];
  }

  set videoThumbnails(val: Thumbnail[]) {
    this._videoThumbnails = val;
  }

  get description() {
    return this._description;
  }

  set description(val: string) {
    this._description = val;
  }

  // get descriptionHtml() {
  //   return this._descriptionHtml;
  // }

  // set descriptionHtml(val: string) {
  //   this._descriptionHtml = val;
  // }

  get viewCount() {
    return this._viewCount;
  }

  set viewCount(val: number) {
    this._viewCount = Number(val);
  }

  // get published() {
  //   return this._published;
  // }

  // set published(val: number) {
  //   this._published = Number(val);
  // }

  // get publishedText() {
  //   return this._publishedText;
  // }

  // set publishedText(val: string) {
  //   this._publishedText = val;
  // }

  get lengthSeconds() {
    return this._lengthSeconds;
  }

  set lengthSeconds(val: number) {
    this._lengthSeconds = Number(val);
  }

  // get liveNow() {
  //   return this._liveNow;
  // }

  // set liveNow(val: boolean) {
  //   this._liveNow = val;
  // }

  // get paid() {
  //   return this._paid;
  // }

  // set paid(val: boolean) {
  //   this._paid = val;
  // }

  // get premium() {
  //   return this._premium;
  // }

  // set premium(val: boolean) {
  //   this._premium = val;
  // }

  create(obj: ITrack) {
    this.id = obj.id;
    this.createdAt = obj.createdAt;
    this.updatedAt = obj.updatedAt;
    this.type = obj.type;
    this.title = obj.title;
    this.author = obj.author;
    this.videoId = obj.videoId;
    this.authorId = obj.authorId;
    this.authorUrl = obj.authorUrl;
    this.videoThumbnails = obj.videoThumbnails;
    this.description = obj.description;
    // this.descriptionHtml = obj.descriptionHtml;
    this.viewCount = obj.viewCount;
    // this.published = obj.published;
    // this.publishedText = obj.publishedText;
    this.lengthSeconds = obj.lengthSeconds;
    // this.liveNow = obj.liveNow;
    // this.paid = obj.paid;
    // this.premium = obj.premium;
    this.likeCount = obj.likeCount;
    this.genre = obj.genre;
    this.genreUrl = obj.genreUrl;
    this.rating = obj.rating;
    this.adaptiveFormats = obj.adaptiveFormats;
    this.formatStreams = obj.formatStreams;
    this.recommendedVideos = obj.recommendedVideos;

    return this;
  }

  extend(obj: ITrack) {
    Object.assign(this, obj);

    return this;
  }

  get hasData() {
    return Boolean(
      this.type &&
        this.title &&
        this.author &&
        this.videoId &&
        this.description,
    );
  }
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
// export class Convert {
//   public static toTrending(json: string): Trending {
//     return cast<Trending>(JSON.parse(json), r('Trending'));
//   }

//   public static trendingToJson(value: Trending): string {
//     return JSON.stringify(uncast(value, r('Trending')), null, 2);
//   }
// }

// function invalidValue(typ: any, val: any): never {
//   throw Error(
//     `Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`,
//   );
// }

// function jsonToJSProps(typ: any): any {
//   if (typ.jsonToJS === undefined) {
//     var map: any = {};
//     typ.props.forEach((p: any) => (map[p.json] = {key: p.js, typ: p.typ}));
//     typ.jsonToJS = map;
//   }
//   return typ.jsonToJS;
// }

// function jsToJSONProps(typ: any): any {
//   if (typ.jsToJSON === undefined) {
//     var map: any = {};
//     typ.props.forEach((p: any) => (map[p.js] = {key: p.json, typ: p.typ}));
//     typ.jsToJSON = map;
//   }
//   return typ.jsToJSON;
// }

// function transform(val: any, typ: any, getProps: any): any {
//   function transformPrimitive(typ: string, val: any): any {
//     if (typeof typ === typeof val) return val;
//     return invalidValue(typ, val);
//   }

//   function transformUnion(typs: any[], val: any): any {
//     // val must validate against one typ in typs
//     var l = typs.length;
//     for (var i = 0; i < l; i++) {
//       var typ = typs[i];
//       try {
//         return transform(val, typ, getProps);
//       } catch (_) {}
//     }
//     return invalidValue(typs, val);
//   }

//   function transformEnum(cases: string[], val: any): any {
//     if (cases.indexOf(val) !== -1) return val;
//     return invalidValue(cases, val);
//   }

//   function transformArray(typ: any, val: any): any {
//     // val must be an array with no invalid elements
//     if (!Array.isArray(val)) return invalidValue('array', val);
//     return val.map(el => transform(el, typ, getProps));
//   }

//   function transformDate(typ: any, val: any): any {
//     if (val === null) {
//       return null;
//     }
//     const d = new Date(val);
//     if (isNaN(d.valueOf())) {
//       return invalidValue('Date', val);
//     }
//     return d;
//   }

//   function transformObject(
//     props: {[k: string]: any},
//     additional: any,
//     val: any,
//   ): any {
//     if (val === null || typeof val !== 'object' || Array.isArray(val)) {
//       return invalidValue('object', val);
//     }
//     var result: any = {};
//     Object.getOwnPropertyNames(props).forEach(key => {
//       const prop = props[key];
//       const v = Object.prototype.hasOwnProperty.call(val, key)
//         ? val[key]
//         : undefined;
//       result[prop.key] = transform(v, prop.typ, getProps);
//     });
//     Object.getOwnPropertyNames(val).forEach(key => {
//       if (!Object.prototype.hasOwnProperty.call(props, key)) {
//         result[key] = transform(val[key], additional, getProps);
//       }
//     });
//     return result;
//   }

//   if (typ === 'any') return val;
//   if (typ === null) {
//     if (val === null) return val;
//     return invalidValue(typ, val);
//   }
//   if (typ === false) return invalidValue(typ, val);
//   while (typeof typ === 'object' && typ.ref !== undefined) {
//     typ = typeMap[typ.ref];
//   }
//   if (Array.isArray(typ)) return transformEnum(typ, val);
//   if (typeof typ === 'object') {
//     return typ.hasOwnProperty('unionMembers')
//       ? transformUnion(typ.unionMembers, val)
//       : typ.hasOwnProperty('arrayItems')
//       ? transformArray(typ.arrayItems, val)
//       : typ.hasOwnProperty('props')
//       ? transformObject(getProps(typ), typ.additional, val)
//       : invalidValue(typ, val);
//   }
//   // Numbers can be parsed by Date but shouldn't be.
//   if (typ === Date && typeof val !== 'number') return transformDate(typ, val);
//   return transformPrimitive(typ, val);
// }

// function cast<T>(val: any, typ: any): T {
//   return transform(val, typ, jsonToJSProps);
// }

// function uncast<T>(val: T, typ: any): any {
//   return transform(val, typ, jsToJSONProps);
// }

// function a(typ: any) {
//   return {arrayItems: typ};
// }

// function u(...typs: any[]) {
//   return {unionMembers: typs};
// }

// function o(props: any[], additional: any) {
//   return {props, additional};
// }

// function m(additional: any) {
//   return {props: [], additional};
// }

// function r(name: string) {
//   return {ref: name};
// }

// const typeMap: any = {
//   Trending: o(
//     [
//       {json: 'type', js: 'type', typ: ''},
//       {json: 'title', js: 'title', typ: ''},
//       {json: 'videoId', js: 'videoId', typ: ''},
//       {json: 'author', js: 'author', typ: ''},
//       {json: 'authorId', js: 'authorId', typ: ''},
//       {json: 'authorUrl', js: 'authorUrl', typ: ''},
//       {
//         json: 'videoThumbnails',
//         js: 'videoThumbnails',
//         typ: a(r('Thumbnail')),
//       },
//       {json: 'description', js: 'description', typ: ''},
//       {json: 'descriptionHtml', js: 'descriptionHtml', typ: ''},
//       {json: 'viewCount', js: 'viewCount', typ: 0},
//       {json: 'published', js: 'published', typ: 0},
//       {json: 'publishedText', js: 'publishedText', typ: ''},
//       {json: 'lengthSeconds', js: 'lengthSeconds', typ: 0},
//       {json: 'liveNow', js: 'liveNow', typ: true},
//       {json: 'paid', js: 'paid', typ: true},
//       {json: 'premium', js: 'premium', typ: true},
//     ],
//     false,
//   ),
//   Thumbnail: o(
//     [
//       {json: 'quality', js: 'quality', typ: ''},
//       {json: 'url', js: 'url', typ: ''},
//       {json: 'width', js: 'width', typ: 0},
//       {json: 'height', js: 'height', typ: 0},
//     ],
//     false,
//   ),
// };
