import {Thumbnail, Quality} from './Thumbnail';
import Domain, {IDomain} from './Domain';
import Track from './Track';

// To parse this data:
//
//   import { Convert, TrackDetail } from "./file";
//
//   const trackDetail = Convert.toTrackDetail(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

// export interface ITrackDetail {
//   type: string;
//   title: string;
//   videoId: string;
//   videoThumbnails: Thumbnail[];
//   storyboards: Storyboard[];
//   description: string;
//   descriptionHtml: string;
//   published: number;
//   publishedText: string;
//   keywords: string[];
//   viewCount: number;
//   likeCount: number;
//   dislikeCount: number;
//   paid: boolean;
//   premium: boolean;
//   isFamilyFriendly: boolean;
//   allowedRegions: string[];
//   genre: string;
//   genreUrl: string;
//   author: string;
//   authorId: string;
//   authorUrl: string;
//   authorThumbnails: Thumbnail[];
//   subCountText: string;
//   lengthSeconds: number;
//   allowRatings: boolean;
//   rating: number;
//   isListed: boolean;
//   liveNow: boolean;
//   isUpcoming: boolean;
//   dashUrl: string;
//   adaptiveFormats: AdaptiveFormat[];
//   formatStreams: FormatStream[];
//   captions: Caption[];
//   recommendedVideos: RecommendedVideo[];
// }

export interface AdaptiveFormat {
  index: string;
  bitrate: string;
  init: string;
  url: string;
  itag: string;
  type: string;
  clen: string;
  lmt: string;
  projectionType: string;
  fps?: number;
  container?: Container;
  encoding?: string;
  resolution?: string;
  qualityLabel?: string;
}

export enum Container {
  M4A = 'm4a',
  Mp4 = 'mp4',
  Webm = 'webm',
}

export interface FormatStream {
  url: string;
  itag: string;
  type: string;
  quality: Quality;
  fps: number;
  container: Container;
  encoding: string;
  resolution: string;
  qualityLabel: string;
  size: string;
}

export interface RecommendedVideo {
  videoId: string;
  title: string;
  videoThumbnails: Thumbnail[];
  author: string;
  authorUrl: null;
  authorId: string;
  authorThumbnails: Thumbnail[];
  lengthSeconds: number;
  viewCountText: string;
  viewCount: number | null;
}

// export interface Storyboard {
//   url: string;
//   templateUrl: string;
//   width: number;
//   height: number;
//   count: number;
//   interval: number;
//   storyboardWidth: number;
//   storyboardHeight: number;
//   storyboardCount: number;
// }

// export interface Caption {
//   label: string;
//   languageCode: string;
//   url: string;
// }

export type NextTrack = Exclude<Track, keyof RecommendedVideo>;

export interface ITrackDetail extends IDomain {
  title: string;
  videoId: string;
  videoThumbnails: Thumbnail[];
  viewCount: number;
  likeCount: number;
  genre: string;
  author: string;
  lengthSeconds: number;
  rating: number;
  adaptiveFormats: AdaptiveFormat[];
  formatStreams: FormatStream[];
  recommendedVideos: NextTrack[];
  description: string;
}

export default class TrackDetail extends Domain implements ITrackDetail {
  private _title!: string;
  private _videoId!: string;
  private _videoThumbnails: Thumbnail[] = [];
  private _viewCount!: number;
  private _likeCount!: number;
  private _genre!: string;
  private _author!: string;
  private _description!: string;
  private _lengthSeconds!: number;
  private _rating!: number;
  private _adaptiveFormats: AdaptiveFormat[] = [];
  private _formatStreams: FormatStream[] = [];
  private _recommendedVideos: NextTrack[] = [];

  get title() {
    return this._title;
  }

  set title(val: string) {
    this._title = val;
  }

  get videoId() {
    return this._videoId;
  }

  set videoId(val: string) {
    this._videoId = val;
  }

  get videoThumbnails() {
    return this._videoThumbnails;
  }

  set videoThumbnails(val: Thumbnail[]) {
    this._videoThumbnails = val;
  }

  get viewCount() {
    return this._viewCount;
  }

  set viewCount(val: number) {
    this._viewCount = Number(val);
  }

  get likeCount() {
    return this._likeCount;
  }

  set likeCount(val: number) {
    this._likeCount = Number(val);
  }

  get genre() {
    return this._genre;
  }

  set genre(val: string) {
    this._genre = val;
  }

  get author() {
    return this._author;
  }

  set author(val: string) {
    this._author = val;
  }

  get description() {
    return this._description;
  }

  set description(val: string) {
    this._description = val;
  }

  get lengthSeconds() {
    return this._lengthSeconds;
  }

  set lengthSeconds(val: number) {
    this._lengthSeconds = Number(val);
  }

  get rating() {
    return this._rating;
  }

  set rating(val: number) {
    this._rating = Number(val);
  }

  get adaptiveFormats() {
    return this._adaptiveFormats;
  }

  set adaptiveFormats(val: AdaptiveFormat[]) {
    this._adaptiveFormats = val;
  }

  get formatStreams() {
    return this._formatStreams;
  }

  set formatStreams(val: FormatStream[]) {
    this._formatStreams = val;
  }

  get recommendedVideos() {
    return this._recommendedVideos;
  }

  set recommendedVideos(val: NextTrack[]) {
    this._recommendedVideos = val;
  }

  get hasData(): boolean {
    return Boolean(
      this.videoId && this.title && this.author,
      // && this.adaptiveFormats.length > 0,
    );
  }

  /**
   * @method create - replace constructor ability
   *
   * @param props - {member of TrackDetail}
   * @returns void
   */
  create(props: ITrackDetail) {
    this.id = props.id;
    this.createdAt = props.createdAt;
    this.updatedAt = props.updatedAt;
    this.videoId = props.videoId;
    this.title = props.title;
    this.viewCount = props.viewCount;
    this.likeCount = props.likeCount;
    this.genre = props.genre;
    this.author = props.author;
    this.lengthSeconds = props.lengthSeconds;
    this.rating = props.rating;
    this.adaptiveFormats = props.adaptiveFormats;
    this.formatStreams = props.formatStreams;
    this.recommendedVideos = props.recommendedVideos;
    this.videoThumbnails = props.videoThumbnails;
    this.description = props.description;

    return this;
  }

  extend(props: ITrackDetail) {
    Object.assign(this, props);

    return this;
  }
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
// export class Convert {
//   public static toTrackDetail(json: string): TrackDetail {
//     return cast(JSON.parse(json), r('TrackDetail'));
//   }

//   public static trackDetailToJson(value: TrackDetail): string {
//     return JSON.stringify(uncast(value, r('TrackDetail')), null, 2);
//   }
// }

// function invalidValue(typ: any, val: any): never {
//   throw Error(
//     `Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`,
//   );
// }

// function jsonToJSProps(typ: any): any {
//   if (typ.jsonToJS === undefined) {
//     var map: any = {};
//     typ.props.forEach((p: any) => (map[p.json] = {key: p.js, typ: p.typ}));
//     typ.jsonToJS = map;
//   }
//   return typ.jsonToJS;
// }

// function jsToJSONProps(typ: any): any {
//   if (typ.jsToJSON === undefined) {
//     var map: any = {};
//     typ.props.forEach((p: any) => (map[p.js] = {key: p.json, typ: p.typ}));
//     typ.jsToJSON = map;
//   }
//   return typ.jsToJSON;
// }

// function transform(val: any, typ: any, getProps: any): any {
//   function transformPrimitive(typ: string, val: any): any {
//     if (typeof typ === typeof val) return val;
//     return invalidValue(typ, val);
//   }

//   function transformUnion(typs: any[], val: any): any {
//     // val must validate against one typ in typs
//     var l = typs.length;
//     for (var i = 0; i < l; i++) {
//       var typ = typs[i];
//       try {
//         return transform(val, typ, getProps);
//       } catch (_) {}
//     }
//     return invalidValue(typs, val);
//   }

//   function transformEnum(cases: string[], val: any): any {
//     if (cases.indexOf(val) !== -1) return val;
//     return invalidValue(cases, val);
//   }

//   function transformArray(typ: any, val: any): any {
//     // val must be an array with no invalid elements
//     if (!Array.isArray(val)) return invalidValue('array', val);
//     return val.map(el => transform(el, typ, getProps));
//   }

//   function transformDate(typ: any, val: any): any {
//     if (val === null) {
//       return null;
//     }
//     const d = new Date(val);
//     if (isNaN(d.valueOf())) {
//       return invalidValue('Date', val);
//     }
//     return d;
//   }

//   function transformObject(
//     props: {[k: string]: any},
//     additional: any,
//     val: any,
//   ): any {
//     if (val === null || typeof val !== 'object' || Array.isArray(val)) {
//       return invalidValue('object', val);
//     }
//     var result: any = {};
//     Object.getOwnPropertyNames(props).forEach(key => {
//       const prop = props[key];
//       const v = Object.prototype.hasOwnProperty.call(val, key)
//         ? val[key]
//         : undefined;
//       result[prop.key] = transform(v, prop.typ, getProps);
//     });
//     Object.getOwnPropertyNames(val).forEach(key => {
//       if (!Object.prototype.hasOwnProperty.call(props, key)) {
//         result[key] = transform(val[key], additional, getProps);
//       }
//     });
//     return result;
//   }

//   if (typ === 'any') return val;
//   if (typ === null) {
//     if (val === null) return val;
//     return invalidValue(typ, val);
//   }
//   if (typ === false) return invalidValue(typ, val);
//   while (typeof typ === 'object' && typ.ref !== undefined) {
//     typ = typeMap[typ.ref];
//   }
//   if (Array.isArray(typ)) return transformEnum(typ, val);
//   if (typeof typ === 'object') {
//     return typ.hasOwnProperty('unionMembers')
//       ? transformUnion(typ.unionMembers, val)
//       : typ.hasOwnProperty('arrayItems')
//       ? transformArray(typ.arrayItems, val)
//       : typ.hasOwnProperty('props')
//       ? transformObject(getProps(typ), typ.additional, val)
//       : invalidValue(typ, val);
//   }
//   // Numbers can be parsed by Date but shouldn't be.
//   if (typ === Date && typeof val !== 'number') return transformDate(typ, val);
//   return transformPrimitive(typ, val);
// }

// function cast<T>(val: any, typ: any): T {
//   return transform(val, typ, jsonToJSProps);
// }

// function uncast<T>(val: T, typ: any): any {
//   return transform(val, typ, jsToJSONProps);
// }

// function a(typ: any) {
//   return {arrayItems: typ};
// }

// function u(...typs: any[]) {
//   return {unionMembers: typs};
// }

// function o(props: any[], additional: any) {
//   return {props, additional};
// }

// function m(additional: any) {
//   return {props: [], additional};
// }

// function r(name: string) {
//   return {ref: name};
// }

// const typeMap: any = {
//   TrackDetail: o(
//     [
//       {json: 'type', js: 'type', typ: ''},
//       {json: 'title', js: 'title', typ: ''},
//       {json: 'videoId', js: 'videoId', typ: ''},
//       {json: 'videoThumbnails', js: 'videoThumbnails', typ: a(r('Thumbnail'))},
//       {json: 'storyboards', js: 'storyboards', typ: a(r('Storyboard'))},
//       {json: 'description', js: 'description', typ: ''},
//       {json: 'descriptionHtml', js: 'descriptionHtml', typ: ''},
//       {json: 'published', js: 'published', typ: 0},
//       {json: 'publishedText', js: 'publishedText', typ: ''},
//       {json: 'keywords', js: 'keywords', typ: a('')},
//       {json: 'viewCount', js: 'viewCount', typ: 0},
//       {json: 'likeCount', js: 'likeCount', typ: 0},
//       {json: 'dislikeCount', js: 'dislikeCount', typ: 0},
//       {json: 'paid', js: 'paid', typ: true},
//       {json: 'premium', js: 'premium', typ: true},
//       {json: 'isFamilyFriendly', js: 'isFamilyFriendly', typ: true},
//       {json: 'allowedRegions', js: 'allowedRegions', typ: a('')},
//       {json: 'genre', js: 'genre', typ: ''},
//       {json: 'genreUrl', js: 'genreUrl', typ: ''},
//       {json: 'author', js: 'author', typ: ''},
//       {json: 'authorId', js: 'authorId', typ: ''},
//       {json: 'authorUrl', js: 'authorUrl', typ: ''},
//       {
//         json: 'authorThumbnails',
//         js: 'authorThumbnails',
//         typ: a(r('Thumbnail')),
//       },
//       {json: 'subCountText', js: 'subCountText', typ: ''},
//       {json: 'lengthSeconds', js: 'lengthSeconds', typ: 0},
//       {json: 'allowRatings', js: 'allowRatings', typ: true},
//       {json: 'rating', js: 'rating', typ: 3.14},
//       {json: 'isListed', js: 'isListed', typ: true},
//       {json: 'liveNow', js: 'liveNow', typ: true},
//       {json: 'isUpcoming', js: 'isUpcoming', typ: true},
//       {json: 'dashUrl', js: 'dashUrl', typ: ''},
//       {
//         json: 'adaptiveFormats',
//         js: 'adaptiveFormats',
//         typ: a(r('AdaptiveFormat')),
//       },
//       {json: 'formatStreams', js: 'formatStreams', typ: a(r('FormatStream'))},
//       {json: 'captions', js: 'captions', typ: a('any')},
//       {
//         json: 'recommendedVideos',
//         js: 'recommendedVideos',
//         typ: a(r('RecommendedVideo')),
//       },
//     ],
//     false,
//   ),
//   AdaptiveFormat: o(
//     [
//       {json: 'index', js: 'index', typ: ''},
//       {json: 'bitrate', js: 'bitrate', typ: ''},
//       {json: 'init', js: 'init', typ: ''},
//       {json: 'url', js: 'url', typ: ''},
//       {json: 'itag', js: 'itag', typ: ''},
//       {json: 'type', js: 'type', typ: ''},
//       {json: 'clen', js: 'clen', typ: ''},
//       {json: 'lmt', js: 'lmt', typ: ''},
//       {json: 'projectionType', js: 'projectionType', typ: ''},
//       {json: 'fps', js: 'fps', typ: u(undefined, 0)},
//       {json: 'container', js: 'container', typ: u(undefined, r('Container'))},
//       {json: 'encoding', js: 'encoding', typ: u(undefined, '')},
//       {json: 'resolution', js: 'resolution', typ: u(undefined, '')},
//       {json: 'qualityLabel', js: 'qualityLabel', typ: u(undefined, '')},
//     ],
//     false,
//   ),
//   Thumbnail: o(
//     [
//       {json: 'url', js: 'url', typ: ''},
//       {json: 'width', js: 'width', typ: 0},
//       {json: 'height', js: 'height', typ: 0},
//       {json: 'quality', js: 'quality', typ: u(undefined, r('Quality'))},
//     ],
//     false,
//   ),
//   FormatStream: o(
//     [
//       {json: 'url', js: 'url', typ: ''},
//       {json: 'itag', js: 'itag', typ: ''},
//       {json: 'type', js: 'type', typ: ''},
//       {json: 'quality', js: 'quality', typ: r('Quality')},
//       {json: 'fps', js: 'fps', typ: 0},
//       {json: 'container', js: 'container', typ: r('Container')},
//       {json: 'encoding', js: 'encoding', typ: ''},
//       {json: 'resolution', js: 'resolution', typ: ''},
//       {json: 'qualityLabel', js: 'qualityLabel', typ: ''},
//       {json: 'size', js: 'size', typ: ''},
//     ],
//     false,
//   ),
//   RecommendedVideo: o(
//     [
//       {json: 'videoId', js: 'videoId', typ: ''},
//       {json: 'title', js: 'title', typ: ''},
//       {json: 'videoThumbnails', js: 'videoThumbnails', typ: a(r('Thumbnail'))},
//       {json: 'author', js: 'author', typ: ''},
//       {json: 'authorUrl', js: 'authorUrl', typ: null},
//       {json: 'authorId', js: 'authorId', typ: ''},
//       {
//         json: 'authorThumbnails',
//         js: 'authorThumbnails',
//         typ: a(r('Thumbnail')),
//       },
//       {json: 'lengthSeconds', js: 'lengthSeconds', typ: 0},
//       {json: 'viewCountText', js: 'viewCountText', typ: ''},
//       {json: 'viewCount', js: 'viewCount', typ: u(0, null)},
//     ],
//     false,
//   ),
//   Storyboard: o(
//     [
//       {json: 'url', js: 'url', typ: ''},
//       {json: 'templateUrl', js: 'templateUrl', typ: ''},
//       {json: 'width', js: 'width', typ: 0},
//       {json: 'height', js: 'height', typ: 0},
//       {json: 'count', js: 'count', typ: 0},
//       {json: 'interval', js: 'interval', typ: 0},
//       {json: 'storyboardWidth', js: 'storyboardWidth', typ: 0},
//       {json: 'storyboardHeight', js: 'storyboardHeight', typ: 0},
//       {json: 'storyboardCount', js: 'storyboardCount', typ: 0},
//     ],
//     false,
//   ),
//   Container: ['m4a', 'mp4', 'webm'],
//   Quality: [
//     'default',
//     'end',
//     'high',
//     'maxres',
//     'maxresdefault',
//     'medium',
//     'middle',
//     'sddefault',
//     'start',
//   ],
// };
