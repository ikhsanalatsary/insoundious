/**
 * @format
 * @flow
 *
 * @description Manage connection & expose our database database
 */
import SQLite from 'react-native-sqlite-storage';
import ModelConfig from './ModelConfig';
import Model from './Model';

//@ts-ignore
let isDev = process.env.NODE_ENV !== 'production';
let _instance: ModelProvider;
export default class ModelProvider {
  _config!: ModelConfig;
  _database: SQLite.SQLiteDatabase | undefined;

  static getInstance() {
    if (_instance && _instance instanceof ModelProvider) {
      return _instance;
    }

    let _config = new ModelConfig({database: '_insoundious.db', debug: isDev});
    _instance = new ModelProvider(_config);

    return _instance;
  }

  constructor(config: ModelConfig) {
    this.config = config;
  }

  open(): Promise<SQLite.SQLiteDatabase> {
    SQLite.DEBUG(this.config.debug);
    SQLite.enablePromise(this.config.asPromise);
    let databaseInstance: SQLite.SQLiteDatabase;

    return SQLite.openDatabase({
      name: this.config.database,
      location: this.config.location,
    })
      .then(db => {
        databaseInstance = db;
        console.log('[db] Database open!');

        // Perform any database initialization or updates, if needed
        const model = new Model();
        return model.init(databaseInstance);
      })
      .then(() => {
        this.database = databaseInstance;
        console.log('ggg ', databaseInstance);
        return databaseInstance;
      });
  }

  close(): Promise<void> {
    if (this.database === undefined) {
      return Promise.reject('[db] Database was not open; unable to close.');
    }
    return this.database.close().then(() => {
      console.log('[db] Database closed.');
      this.database = undefined;
    });
  }

  async getDatabase(): Promise<SQLite.SQLiteDatabase> {
    if (this.database === undefined) {
      return this.open();
    }

    return this.database;
  }

  get config(): ModelConfig {
    return this._config;
  }

  set config(value: ModelConfig) {
    this._config = value;
  }

  get database() {
    return this._database;
  }

  set database(value: SQLite.SQLiteDatabase | undefined) {
    this._database = value;
  }
}
