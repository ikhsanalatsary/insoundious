/**
 *
 * @format
 * @flow
 */

import Track, {OmitMethodTrack, ITrack} from 'src/domains/Track';
import ModelProvider from 'src/model/ModelProvider';
import snakeCase from 'lodash/snakeCase';
import isEmpty from 'lodash/isEmpty';
import camelcaseKeys from 'camelcase-keys';
import {NextTrack} from 'src/domains/TrackDetail';

export default class TrackRepository {
  static tableName = 'track';
  private _model!: ModelProvider;

  constructor(model: ModelProvider) {
    this.model = model;
  }

  async create(domain: Track | NextTrack): Promise<void> {
    // Object.assign works in our Domain is because AbstractObject 'toJSON()' method
    // make sure the domain is instance of Track Domain
    // console.log('[db] create ', JSON.stringify(domain, null, 2));
    let {tableName} = TrackRepository;
    let track = new Track().create(domain);
    track.createdAt = new Date().toISOString();
    let remain = Track.serialize(track);
    let columns: string[] = [];
    let values: any[] = [];
    for (const key in remain) {
      if (remain.hasOwnProperty(key)) {
        let value = remain[key as keyof OmitMethodTrack];
        if (
          key === 'videoThumbnails' ||
          key === 'authorThumbnails' ||
          key === 'adaptiveFormats' ||
          key === 'formatStreams' ||
          key === 'recommendedVideos'
        ) {
          // getDatabase().then(db => db.doesn't support JSON / Array JSON
          // so, we need convert the array to string eg: (from [1, 2] to '1,2')
          value = JSON.stringify(value);
        }
        values.push(value);
        // convert key to match every columns in track table eg: firstName to first_name
        columns.push(snakeCase(key));
      }
    }
    let column = columns.join(', ');
    // for values placeholder ?,?,?
    let statementValues = Array.from(
      {length: values.length},
      () => '?',
    ).toString();
    // console.log('columns ', columns);
    console.log('values ', values);
    const sql = `INSERT INTO ${tableName} (${column}) VALUES (${statementValues});`;
    // console.log('sql ', sql);
    const db = await this.model.getDatabase();
    // console.log('db ', db);
    // console.log(
    //   'executeSql ',
    //   values.length,
    //   columns.length,
    //   statementValues.split(',').length,
    // );
    const [results] = await db.executeSql(sql, values);
    // console.log('resultsvv ', results);
    const {insertId} = results;
    console.log('[db] results ', JSON.stringify(results, null, 2));
    console.log(
      `[db] Added ${tableName} ${JSON.stringify(
        remain,
      )}! InsertId: ${insertId}`,
    );
  }

  async findAll(params?: {
    groupBy?: string[];
    limit?: number;
    order?: Map<string, string>;
  }): Promise<Track[]> {
    let {tableName} = TrackRepository;
    // Get all the lists, ordered by newest lists first
    console.log('[db] Fetching lists from the db...');
    let sql = `SELECT * FROM ${tableName}`;
    params = {
      order: new Map([['created_at', 'DESC']]),
      ...params,
    };
    if (params.groupBy) {
      sql += ` GROUP BY ${params.groupBy.toString()}`;
    }
    if (params.order) {
      let key = Array.from(params.order.keys())[0];
      sql += ` ORDER BY ${key} ${params.order.get(key)}`;
    }
    if (params.limit) {
      sql += ` LIMIT ${params.limit}`;
    }

    console.log('sql ', sql);

    const db = await this.model.getDatabase();
    const [results] = await db.executeSql(sql);
    if (results === undefined) {
      return [];
    }
    const count = results.rows.length;
    const lists: Track[] = [];
    for (let i = 0; i < count; i++) {
      let row = results.rows.item(i);
      let newRow: {[key: string]: any} = camelcaseKeys(row);
      //@ts-ignore
      newRow.videoThumbnails = JSON.parse(newRow.videoThumbnails);
      newRow.authorThumbnails = JSON.parse(newRow.authorThumbnails);
      newRow.adaptiveFormats = JSON.parse(newRow.adaptiveFormats);
      newRow.formatStreams = JSON.parse(newRow.formatStreams);
      newRow.recommendedVideos = JSON.parse(newRow.recommendedVideos);
      const track: Track = new Track().create((newRow as unknown) as ITrack);
      lists.push(track);
    }
    // console.log(`[db] List ${JSON.stringify(lists, null, 2)}`);
    console.log(`[db] List length ${lists.length}`);
    return lists;
  }

  async findOne(id: number): Promise<null | Track> {
    console.log('[db] Fetching data by id from the db...');
    const {tableName} = TrackRepository;
    const sql = `SELECT * FROM ${tableName} WHERE id = ${id};`;
    const db = await this.model.getDatabase();
    const [results] = await db.executeSql(sql);
    console.log('[db] findOne results ', results);
    if (results === undefined) {
      return null;
    }
    const row = results.rows.item(0);
    console.log(`[db] findOne(${id}) ${JSON.stringify(row)}`);
    if (row === undefined) {
      return null;
    }
    let newRow: {[key: string]: any} = camelcaseKeys(row);
    //@ts-ignore
    newRow.videoThumbnails = JSON.parse(newRow.videoThumbnails);
    newRow.authorThumbnails = JSON.parse(newRow.authorThumbnails);
    newRow.adaptiveFormats = JSON.parse(newRow.adaptiveFormats);
    newRow.formatStreams = JSON.parse(newRow.formatStreams);
    newRow.recommendedVideos = JSON.parse(newRow.recommendedVideos);
    return new Track().create((newRow as unknown) as ITrack);
  }

  async findOneByVideoId(videoId: string): Promise<null | Track> {
    console.log('[db] Fetching data by videoId from the db...');
    const {tableName} = TrackRepository;
    const sql = `SELECT * FROM ${tableName} WHERE video_id = ?;`;
    const db = await this.model.getDatabase();
    const [results] = await db.executeSql(sql, [videoId]);
    console.log('[db] findOne results ', results);
    if (results === undefined) {
      return null;
    }
    const row = results.rows.item(0);
    console.log(`[db] findOneByVideoId(${videoId}) ${JSON.stringify(row)}`);
    if (row === undefined) {
      return null;
    }
    let newRow: {[key: string]: any} = camelcaseKeys(row);
    //@ts-ignore
    newRow.videoThumbnails = JSON.parse(newRow.videoThumbnails);
    newRow.authorThumbnails = JSON.parse(newRow.authorThumbnails);
    newRow.adaptiveFormats = JSON.parse(newRow.adaptiveFormats);
    newRow.formatStreams = JSON.parse(newRow.formatStreams);
    newRow.recommendedVideos = JSON.parse(newRow.recommendedVideos);
    return new Track().create((newRow as unknown) as ITrack);
  }

  async update(id: number, domain: Track): Promise<boolean> {
    console.log('[db] Updating data by id from the db...');
    const {tableName} = TrackRepository;
    let values = [
      domain.type,
      domain.title,
      domain.videoId,
      domain.author,
      domain.authorId,
      domain.authorUrl,
      JSON.stringify(domain.authorThumbnails),
      JSON.stringify(domain.videoThumbnails),
      domain.description,
      domain.viewCount,
      domain.lengthSeconds,
      domain.likeCount,
      domain.genre,
      domain.genreUrl,
      domain.rating,
      JSON.stringify(domain.adaptiveFormats),
      JSON.stringify(domain.formatStreams),
      JSON.stringify(domain.recommendedVideos),
      new Date().toISOString(),
      id,
    ];
    const sql = `
      UPDATE
        ${tableName}
      SET
        type = ?,
        title =  ?,
        video_id = ?,
        author = ?,
        author_id = ?,
        author_url = ?,
        author_thumbnails = ?,
        video_thumbnails = ?,
        description = ?,
        view_count = ?,
        length_seconds = ?,
        like_count = ?,
        genre = ?,
        genre_url = ?,
        rating = ?,
        adaptive_formats = ?,
        format_streams = ?,
        recommended_videos = ?,
        updated_at = ?
      WHERE
        id = ?;
    `;
    const db = await this.model.getDatabase();
    const [results] = await db.executeSql(sql, values);
    console.log('[db] Updated success!');
    console.log('[db] update results ', results);
    return true;
  }

  async delete(id: number): Promise<boolean> {
    console.log(`[db] Deleting track with id: ${id}`);
    const {tableName} = TrackRepository;
    const db = await this.model.getDatabase();
    await db.executeSql(`DELETE FROM ${tableName} WHERE id = ?;`, [id]);
    console.log('[db] Deleted success!');
    return true;
  }

  async findAllArtists(params?: {limit?: number}): Promise<Track[]> {
    return this.findAll({groupBy: ['author'], ...params});
  }

  async count(where: Partial<Track>): Promise<number> {
    const {tableName} = TrackRepository;
    let mapWhere = Object.keys(where)
      .map((val, index, arr) => `${val} = ?${index === arr.length ? '' : ','}`)
      .join();
    let values = Object.values(where);
    let selection = 'COUNT(*)';
    let sql = `SELECT ${selection} FROM ${tableName}`;
    if (!isEmpty(where)) {
      sql += ` WHERE ${mapWhere};`;
    }
    const db = await this.model.getDatabase();
    const results = await db.executeSql(
      sql,
      values.length > 0 ? values : undefined,
    );
    console.log('mapWhere ', mapWhere);
    console.log('values ', values);
    console.log('results ', results[0].rows.raw());
    // console.log('results ', results[0].rows.length);
    return results[0].rows.raw()[0][selection];
  }

  async deleteAll() {
    console.log('[db] Deleting all tracks');
    const {tableName} = TrackRepository;
    const db = await this.model.getDatabase();
    await db.executeSql(`DELETE FROM ${tableName};`);
    console.log('[db] All Deleted!');
    return true;
  }

  get model() {
    return this._model;
  }

  set model(value: ModelProvider) {
    this._model = value;
  }
}
