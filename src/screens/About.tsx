import * as React from 'react';
import {StyleSheet, View, Linking, StatusBar} from 'react-native';
import {
  Avatar,
  Text,
  Title,
  Paragraph,
  Appbar,
  Colors,
  overlay,
  withTheme,
} from 'react-native-paper';
import {ScrollView} from 'react-native-gesture-handler';
import {AppBarHeader} from 'src/components/AppBar';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp, useFocusEffect} from '@react-navigation/native';
import {LibraryStackParamList} from 'src/Router';
import {elevation} from 'src/themes';
import {Theme} from 'react-native-paper/lib/typescript/src/types';

type AboutNavigationProp = StackNavigationProp<LibraryStackParamList, 'About'>;
type AboutRouteProp = RouteProp<LibraryStackParamList, 'About'>;

interface AboutProps {
  navigation: AboutNavigationProp;
  theme: Theme;
}

const About = ({navigation, theme}: AboutProps) => {
  let email = 'shadow.monarch.arise@gmail.com';
  useFocusEffect(() => {
    StatusBar.setBackgroundColor(
      // @ts-ignore
      theme.dark
        ? overlay(elevation, theme.colors.surface)
        : theme.colors.primary,
      true,
    );
  });

  return (
    <>
      <AppBarHeader>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title="About Insoundious" />
      </AppBarHeader>
      <ScrollView contentContainerStyle={styles.wrapper}>
        <View style={styles.logo}>
          <Avatar.Image
            source={require('../assets/icons/play.png')}
            size={100}
            style={styles.image}
          />
          <Title>Insoundious</Title>
          <Text>version {require('../../app.json').version}</Text>
        </View>
        <View style={styles.content}>
          <Text>Youtube audio streaming on Android</Text>
          <Title style={styles.title}>Current Limitation</Title>
          <Paragraph>
            These points are currently not supported by the app. Will coming in
            the next release
          </Paragraph>
          <Paragraph>1. Creating a playlist</Paragraph>
          <Paragraph>2. Importing youtube playlist via url</Paragraph>
          <Paragraph>3. Displaying a playlist from search result</Paragraph>
          <Paragraph>4. Repeat and shuffle tracks</Paragraph>
          <Paragraph>5. Auto play recommended videos</Paragraph>
          <Paragraph>6. Offline mode</Paragraph>
          <Title style={styles.title}>Contact Developer</Title>
          <Paragraph>If you have any feedback. Please tell us:</Paragraph>
          <Paragraph
            style={{color: Colors.lightBlue500}}
            onPress={() => {
              Linking.openURL(`mailto:${email}`);
            }}>
            {email}
          </Paragraph>
        </View>
      </ScrollView>
    </>
  );
};

export default withTheme(About);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
    marginHorizontal: 8,
    paddingBottom: 80,
  },
  image: {
    margin: 4,
  },
  logo: {
    alignItems: 'center',
  },
  content: {
    marginTop: 16,
  },
  title: {marginTop: 12, fontSize: 18},
});
