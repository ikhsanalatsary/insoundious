import React, {useState, useCallback} from 'react';
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  StatusBar,
} from 'react-native';
import {
  Avatar,
  Appbar,
  ActivityIndicator,
  Text,
  Button,
  overlay,
  withTheme,
} from 'react-native-paper';
import {FlatGrid} from 'react-native-super-grid';
import {useFocusEffect} from '@react-navigation/native';
import AppService from 'src/services/AppService';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import {LibraryStackParamList} from 'src/Router';
import Track from 'src/domains/Track';
import {AppBarHeader} from 'src/components/AppBar';
import {placeholderAuthor, elevation} from 'src/themes';
import {Theme} from 'react-native-paper/lib/typescript/src/types';

const {ConnectionState} = AppService;

type ArtistNavigationProp = StackNavigationProp<
  LibraryStackParamList,
  'Artist'
>;
type ArtistRouteProp = RouteProp<LibraryStackParamList, 'Artist'>;

type Props = {
  navigation: ArtistNavigationProp;
  theme: Theme;
};
const offset = 10;
const Artist = ({navigation, theme}: Props) => {
  let [tracks, setTrack] = useState<Track[]>([]);
  let [conState, setConnectionState] = useState(ConnectionState.NONE);
  let [limit, setLimit] = useState(offset);
  let [count, setCount] = useState(0);
  useFocusEffect(
    useCallback(() => {
      // Do something when the screen is focused
      setConnectionState(ConnectionState.LOADING);
      console.log('limit ', limit);
      AppService.trackService.countRecord().then(setCount);
      AppService.trackService
        .findAllArtists({limit})
        .then(res => {
          let tracks = res.map(obj => {
            let track = new Track();
            return track.create(obj);
          });
          setTrack(tracks);
          setConnectionState(ConnectionState.SUCCESS);
        })
        .catch(() => setConnectionState(ConnectionState.ERROR));

      StatusBar.setBackgroundColor(
        // @ts-ignore
        theme.dark
          ? overlay(elevation, theme.colors.surface)
          : theme.colors.primary,
        true,
      );

      return () => {
        // Do something when the screen is unfocused
        // Useful for cleanup functions
      };
    }, [limit]),
  );

  let EmptyComponent =
    conState === ConnectionState.NONE ||
    conState === ConnectionState.LOADING ? (
      <ActivityIndicator size="large" animating={true} />
    ) : null;
  return (
    <>
      <AppBarHeader>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title="Publisher" />
      </AppBarHeader>
      <FlatGrid
        extraData={limit}
        style={styles.list}
        contentContainerStyle={styles.contentList}
        itemDimension={130}
        items={tracks}
        listKey="videoId"
        ListEmptyComponent={() => EmptyComponent}
        ListFooterComponent={() =>
          count > limit ? (
            <Button
              compact
              style={styles.btn}
              icon="more"
              mode="contained"
              onPress={() => setLimit(limit + offset)}>
              Load More
            </Button>
          ) : null
        }
        renderItem={({item, index}) => {
          let thumbnail = item.authorThumbnails.find(v => v.width === 512);
          // console.log({thumbnail: item.authorThumbnails});
          if (item.authorThumbnails.length === 0) {
            thumbnail = item.videoThumbnails.find(v => v.quality === 'medium');
          }
          // console.log(item.authorThumbnails);
          return (
            <TouchableWithoutFeedback
              onPress={() =>
                navigation.navigate('PlayByArtist', {
                  author: item.author,
                  authorId: item.authorId,
                })
              }>
              <View style={styles.item}>
                <Avatar.Image
                  size={150}
                  source={{uri: thumbnail?.url ?? placeholderAuthor}}
                />
                <Text>{item.author}</Text>
              </View>
            </TouchableWithoutFeedback>
          );
        }}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    // marginTop: 50,
    marginBottom: 50, // TODO: change this dynamically
  },
  contentList: {
    paddingBottom: 20,
  },
  item: {
    alignItems: 'center',
    flexGrow: 1,
  },
  btn: {
    width: 150,
    alignSelf: 'center',
  },
});

export default withTheme(Artist);
