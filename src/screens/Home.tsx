import React, {useCallback, useState, useEffect} from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  ScrollView,
  StyleProp,
  StatusBar,
} from 'react-native';
import {
  Headline,
  withTheme,
  overlay,
  Caption,
  Button,
  // Text,
  Surface,
  ActivityIndicator,
  // Switch,
  Appbar,
  Colors,
} from 'react-native-paper';
import {useFocusEffect, RouteProp} from '@react-navigation/native';
// import BottomSheet from 'reanimated-bottom-sheet';
import {elevation} from 'src/themes';
import AppService from 'src/services/AppService';
import Track from 'src/domains/Track';
import {StackNavigationProp} from '@react-navigation/stack';
import {StackParamList} from 'src/Router';
import {AppBar} from 'src/components/AppBar';
import TrackItem from 'src/components/TrackItem';
// import Empty from 'src/components/Empty';
import Animated from 'react-native-reanimated';
import {useAppState, useAppDispatch, CHANGE_THEME} from 'src/Context';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Setting from 'src/domains/Setting';
import {Theme} from 'react-native-paper/lib/typescript/src/types';

type HomeNavigationProp = StackNavigationProp<StackParamList, 'Home'>;
type HomeRouteProp = RouteProp<StackParamList, 'Home'>;
type Props = {
  navigation: HomeNavigationProp;
  theme: Theme;
  route: HomeRouteProp;
};
export default React.memo(
  withTheme(function Home({theme, navigation}: Props) {
    let backgroundColor = theme.dark
      ? overlay(elevation, theme.colors.surface)
      : Colors.white;
    let [trendings, setTrending] = useState<Track[]>([]);
    let [recents, setRecent] = useState<Track[]>([]);
    let [count, setCount] = useState(0);
    // let bs = useRef();
    // let ref = useRef();
    let {isDarkMode} = useAppState();
    let setTheme = useAppDispatch();
    let limit = 5;
    // let fall = new Animated.Value(1);
    useFocusEffect(
      useCallback(() => {
        // Do something when the screen is focused
        AppService.getTrending().then(res => {
          let tracks = res.map(obj => {
            let track = new Track();
            return track.create(obj);
          });
          setTrending(tracks);
        });
        AppService.trackService.countRecord().then(setCount);

        AppService.trackService.findAllTracks({limit}).then(res => {
          let tracks = res.map(obj => {
            let track = new Track();
            return track.create(obj);
          });
          setRecent(tracks);
        });
        StatusBar.setBackgroundColor(
          // @ts-ignore
          theme.dark
            ? overlay(elevation, theme.colors.surface)
            : Colors.grey400,
          true,
        );

        return () => {
          // Do something when the screen is unfocused
          // Useful for cleanup functions
        };
      }, [limit]),
    );

    useEffect(() => {
      StatusBar.setBackgroundColor(
        // @ts-ignore
        theme.dark ? overlay(elevation, theme.colors.surface) : Colors.grey400,
        true,
      );
    }, [theme]);
    let onNavigate = (item: Track, request = true) => {
      navigation.push('TrackDetails', {
        videoId: item.videoId,
        data: Track.serialize(item),
        request,
      });
    };

    // TODO:  @ts-ignore not support jsx comment
    return (
      <>
        <AppBar
          style={[
            styles.appbar,
            // do this because appbar will overide all style
            {
              backgroundColor,
            } as StyleProp<object>,
          ]}>
          {/* <Appbar.Action
          icon="settings"
          onPress={() => {
            if (!ref.current || ref.current === 0) {
              ref.current = 1; // lowest
            } else {
              ref.current = 0; // highest
            }
            console.log('ref.current ', ref.current);
            // bs.current?.snapTo?.(0);
            bs.current?.snapTo?.(ref.current);
          }}
        /> */}
          <Appbar.Action
            icon={props => {
              return <Ionicons {...props} name="md-moon" />;
            }}
            onPress={() => {
              let dark = !isDarkMode;
              AppService.storageService
                .setSetting(new Setting().create({isDarkMode: dark}))
                .then(console.log);
              setTheme({type: CHANGE_THEME, isDarkMode: dark});
            }}
          />
        </AppBar>
        <ScrollView>
          <Animated.View
            style={[
              styles.column,
              // {
              //   opacity: Animated.add(0.1, Animated.multiply(fall, 0.9)),
              // },
            ]}>
            <Headline>Recently Played</Headline>
            <FlatList
              data={recents}
              ListEmptyComponent={() => (
                <View>
                  <Caption>No recently played</Caption>
                </View>
              )}
              horizontal={true}
              style={styles.row}
              keyExtractor={item => item.videoId}
              // eslint-disable-next-line @typescript-eslint/no-unused-vars
              renderItem={({item, index}) => (
                <TrackItem
                  item={item}
                  horizontal={true}
                  backgroundColor={backgroundColor}
                  onNavigate={() => {
                    onNavigate(item, false);
                  }}
                />
              )}
              ListFooterComponent={() =>
                count > limit ? (
                  <Button
                    compact
                    contentStyle={styles.surface}
                    icon="arrow-right"
                    mode="text"
                    onPress={() =>
                      navigation.navigate('LastPlayed', {
                        title: 'Recently Played',
                      })
                    }>
                    View More
                  </Button>
                ) : null
              }
            />
            <Headline>Top Tracks</Headline>
            <FlatList
              data={trendings}
              horizontal={true}
              style={styles.row}
              keyExtractor={item => item.videoId}
              // eslint-disable-next-line @typescript-eslint/no-unused-vars
              renderItem={({item, index}) => (
                <TrackItem
                  item={item}
                  horizontal={true}
                  backgroundColor={backgroundColor}
                  onNavigate={() => {
                    onNavigate(item);
                  }}
                />
              )}
              ListEmptyComponent={() => (
                <Surface style={styles.empty}>
                  <ActivityIndicator size="large" animating={true} />
                </Surface>
              )}
            />
          </Animated.View>
          {/* <BottomSheet
          ref={bs}
          enabledInnerScrolling={false}
          snapPoints={[500, 50]}
          initialSnap={1}
          renderContent={() => (
            <View
              style={[
                styles.panel,
                {
                  backgroundColor,
                },
              ]}>
              <View style={styles.switch}>
                <Text>Dark Mode</Text>
                <Switch
                  value={false}
                  onValueChange={() => {
                    console.log('asd');
                    setTheme({type: CHANGE_THEME});
                  }}
                />
              </View>
              <Button mode="contained" onPress={() => console.log('haha')}>
                asdas
              </Button>
            </View>
          )}
        /> */}
        </ScrollView>
      </>
    );
  }),
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 8,
  },
  column: {
    flexDirection: 'column',
  },
  appbar: {
    justifyContent: 'flex-end',
    elevation: 0,
  },
  card: {
    marginHorizontal: 8,
    height: 300,
    width: 200,
    flexWrap: 'wrap',
  },
  row: {
    marginTop: 8,
    marginBottom: 60,
  },
  cover: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
  },
  surface: {
    height: 200,
    width: 120,
  },
  // switch: {
  //   flexDirection: 'row',
  //   justifyContent: 'space-between',
  // },
  empty: {height: 300, width: 200, marginTop: 8},
  //dk
  // box: {
  //   width: IMAGE_SIZE,
  //   height: IMAGE_SIZE,
  // },
  // panelContainer: {
  //   position: 'absolute',
  //   top: 0,
  //   bottom: 0,
  //   left: 0,
  //   right: 0,
  // },
  // panel: {
  //   position: 'absolute',
  //   top: 0,
  //   bottom: 0,
  //   left: 0,
  //   right: 0,
  //   zIndex: 1000,
  //   height: 600,
  //   padding: 20,
  //   backgroundColor: '#2c2c2fAA',
  //   paddingTop: 20,
  //   borderTopLeftRadius: 20,
  //   borderTopRightRadius: 20,
  //   shadowColor: '#000000',
  //   shadowOffset: {width: 0, height: 0},
  //   shadowRadius: 5,
  //   shadowOpacity: 0.4,
  // },
  // header: {
  //   width: '100%',
  //   height: 50,
  // },
  // panelHeader: {
  //   alignItems: 'center',
  // },
  // panelHandle: {
  //   width: 40,
  //   height: 8,
  //   borderRadius: 4,
  //   backgroundColor: '#00000040',
  //   marginBottom: 10,
  // },
  // panelTitle: {
  //   fontSize: 27,
  //   height: 35,
  // },
  // panelSubtitle: {
  //   fontSize: 14,
  //   color: 'gray',
  //   height: 30,
  //   marginBottom: 10,
  // },
  // panelButton: {
  //   padding: 20,
  //   borderRadius: 10,
  //   backgroundColor: '#292929',
  //   alignItems: 'center',
  //   marginVertical: 10,
  // },
  // panelButtonTitle: {
  //   fontSize: 17,
  //   fontWeight: 'bold',
  //   color: 'white',
  // },
  // photo: {
  //   width: '100%',
  //   height: 225,
  //   marginTop: 30,
  // },
  // map: {
  //   height: '100%',
  //   width: '100%',
  // },
});
