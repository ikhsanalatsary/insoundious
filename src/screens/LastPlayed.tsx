import React, {useCallback, useState} from 'react';
import {StyleSheet, FlatList, StatusBar} from 'react-native';
import {
  List,
  Divider,
  Avatar,
  Appbar,
  Button,
  Portal,
  Modal,
  Card,
  overlay,
  withTheme,
} from 'react-native-paper';
import Track from 'src/domains/Track';
import {placeholderImage, elevation} from 'src/themes';
import {useFocusEffect} from '@react-navigation/native';
import AppService from 'src/services/AppService';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import {StackParamList} from 'src/Router';
import {AppBarHeader} from 'src/components/AppBar';
import {Theme} from 'react-native-paper/lib/typescript/src/types';

type LastPlayedNavigationProp = StackNavigationProp<
  StackParamList,
  'LastPlayed'
>;
type LastPlayedRouteProp = RouteProp<StackParamList, 'LastPlayed'>;

type Props = {
  navigation: LastPlayedNavigationProp;
  route: LastPlayedRouteProp;
  theme: Theme;
};

const offset = 10;
const LastPlayed = ({navigation, route, theme}: Props) => {
  let {title} = route.params;
  let [tracks, setTrack] = useState<Track[]>([]);
  let [limit, setLimit] = useState(offset);
  let [count, setCount] = useState(0);
  let [visibleModal, setVisibleModal] = useState(false);
  useFocusEffect(
    useCallback(() => {
      // Do something when the screen is focused
      StatusBar.setBackgroundColor(
        // @ts-ignore
        theme.dark
          ? overlay(elevation, theme.colors.surface)
          : theme.colors.primary,
        true,
      );
      // console.log('limit ', limit);
      AppService.trackService.countRecord().then(setCount);

      AppService.trackService.findAllTracks({limit}).then(res => {
        let tracks = res.map(obj => {
          let track = new Track();
          return track.create(obj);
        });
        setTrack(tracks);
      });

      return () => {
        // Do something when the screen is unfocused
        // Useful for cleanup functions
      };
    }, [limit]),
  );

  return (
    <>
      <Portal>
        <Modal visible={visibleModal} onDismiss={() => setVisibleModal(false)}>
          <Card style={{width: 250, alignSelf: 'center'}}>
            <Card.Title
              title="All data will be deleted"
              subtitle="This action cannot undo!, Are you sure?"
            />
            <Card.Actions style={{justifyContent: 'flex-end'}}>
              <Button onPress={() => setVisibleModal(false)}>Cancel</Button>
              <Button
                onPress={() => {
                  AppService.trackService
                    .reset()
                    .then(() => setLimit(limit + 10));
                  setVisibleModal(false);
                }}>
                Ok
              </Button>
            </Card.Actions>
          </Card>
        </Modal>
      </Portal>
      <AppBarHeader>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title={title} />
        <Appbar.Action
          icon="database-remove"
          onPress={() => setVisibleModal(true)}
        />
      </AppBarHeader>
      <FlatList
        style={styles.list}
        contentContainerStyle={styles.contentList}
        data={tracks}
        extraData={limit}
        keyExtractor={item => item.videoId}
        ItemSeparatorComponent={() => <Divider />}
        ListFooterComponent={() =>
          count > limit ? (
            <Button
              compact
              style={styles.btn}
              icon="more"
              mode="contained"
              onPress={() => setLimit(limit + offset)}>
              Load More
            </Button>
          ) : null
        }
        renderItem={({item}) => {
          let thumbnail = item.videoThumbnails.find(
            v => v.quality === 'medium',
          );
          return (
            <List.Item
              onPress={() => {
                navigation.push('TrackDetails', {
                  videoId: item.videoId,
                  data: Track.serialize(new Track().create(item)),
                  request: false,
                });
              }}
              title={item.title}
              left={props => (
                <Avatar.Image
                  {...props}
                  size={50}
                  source={{uri: thumbnail?.url ?? placeholderImage}}
                />
              )}
            />
          );
        }}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    marginBottom: 50, // TODO: change this dynamically
  },
  contentList: {
    paddingBottom: 20,
  },
  btn: {
    width: 150,
    alignSelf: 'center',
  },
});

export default withTheme(LastPlayed);
