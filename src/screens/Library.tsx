import React from 'react';
import {StyleSheet, StatusBar} from 'react-native';
import {List, Divider, overlay, Colors, withTheme} from 'react-native-paper';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp, useFocusEffect} from '@react-navigation/native';
import {LibraryStackParamList} from 'src/Router';
import {SafeAreaLayout, SaveAreaInset} from 'src/components/SafeAreaLayout';
import {elevation} from 'src/themes';
import {Theme} from 'react-native-paper/lib/typescript/src/types';

type LibraryNavigationProp = StackNavigationProp<
  LibraryStackParamList,
  'Library'
>;
type LibraryRouteProp = RouteProp<LibraryStackParamList, 'Library'>;

type Props = {
  navigation: LibraryNavigationProp;
  theme: Theme;
};
const Library = ({navigation, theme}: Props) => {
  useFocusEffect(() => {
    StatusBar.setBackgroundColor(
      // @ts-ignore
      theme.dark ? overlay(elevation, theme.colors.surface) : Colors.grey400,
      true,
    );
  });
  return (
    <SafeAreaLayout style={styles.container} insets={SaveAreaInset.TOP}>
      {/* <List.Item
        title="PlayList"
        right={props => <List.Icon {...props} icon="arrow-right" />}
      />
      <Divider /> */}
      <List.Item
        title="Publisher"
        right={props => <List.Icon {...props} icon="arrow-right" />}
        onPress={() => navigation.navigate('Artist')}
      />
      <Divider />
      <List.Item
        title="Recent"
        right={props => <List.Icon {...props} icon="arrow-right" />}
        onPress={() => navigation.navigate('LastPlayed', {title: 'Recent'})}
      />
      <Divider />
      <List.Item
        title="Now Playing"
        right={props => <List.Icon {...props} icon="arrow-right" />}
        onPress={() => navigation.navigate('Queue')}
      />
      <Divider />
      <List.Item
        title="About"
        right={props => <List.Icon {...props} icon="arrow-right" />}
        onPress={() => {
          navigation.navigate('About');
        }}
      />
      <Divider />
    </SafeAreaLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default withTheme(Library);
