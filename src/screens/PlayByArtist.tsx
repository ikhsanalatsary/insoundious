import React, {useCallback, useState} from 'react';
import {StyleSheet, FlatList, StatusBar} from 'react-native';
import {
  List,
  Divider,
  Avatar,
  Appbar,
  overlay,
  withTheme,
} from 'react-native-paper';
import Track from 'src/domains/Track';
import {placeholderImage, theme, elevation} from 'src/themes';
import {useFocusEffect} from '@react-navigation/native';
import AppService from 'src/services/AppService';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import {LibraryStackParamList} from 'src/Router';
import {AppBarHeader} from 'src/components/AppBar';
import Loading from 'src/components/Loading';
import {Theme} from 'react-native-paper/lib/typescript/src/types';

type PlayByArtistNavigationProp = StackNavigationProp<
  LibraryStackParamList,
  'PlayByArtist'
>;
type PlayByArtistRouteProp = RouteProp<LibraryStackParamList, 'PlayByArtist'>;

type Props = {
  navigation: PlayByArtistNavigationProp;
  route: PlayByArtistRouteProp;
  theme: Theme;
};

const offset = 10;
const PlayByArtist = ({navigation, route, theme}: Props) => {
  let data = route.params;
  let [tracks, setTrack] = useState<Track[]>([]);
  useFocusEffect(
    useCallback(() => {
      // Do something when the screen is focused
      StatusBar.setBackgroundColor(
        // @ts-ignore
        theme.dark
          ? overlay(elevation, theme.colors.surface)
          : theme.colors.primary,
        true,
      );

      AppService.getPublishedByArtist(data.authorId).then(res => {
        let tracks = res.map(obj => {
          let track = new Track();
          return track.create(obj);
        });
        setTrack(tracks);
      });

      return () => {
        // Do something when the screen is unfocused
        // Useful for cleanup functions
      };
    }, []),
  );

  return (
    <>
      <AppBarHeader>
        <Appbar.BackAction
          onPress={() => {
            AppService.abortController?.abort();
            navigation.goBack();
          }}
        />
        <Appbar.Content title={`Published by ${data.author}`} />
      </AppBarHeader>
      <FlatList
        style={styles.list}
        data={tracks}
        keyExtractor={item => item.videoId}
        ItemSeparatorComponent={() => <Divider />}
        ListEmptyComponent={Loading}
        renderItem={({item, index}) => {
          let thumbnail = item.videoThumbnails.find(
            v => v.quality === 'medium',
          );
          return (
            <List.Item
              onPress={() => {
                navigation.push('TrackDetails', {
                  videoId: item.videoId,
                  data: Track.serialize(new Track().create(item)),
                  request: false,
                });
              }}
              title={item.title}
              left={props => (
                <Avatar.Image
                  {...props}
                  size={50}
                  source={{uri: thumbnail?.url ?? placeholderImage}}
                />
              )}
            />
          );
        }}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    marginBottom: 50,
  },
  btn: {
    width: 150,
    alignSelf: 'center',
  },
});

export default withTheme(PlayByArtist);
