/* eslint-disable react-native/no-inline-styles */
/**
 * @format
 */
import React, {useState, useCallback, useEffect, useRef} from 'react';
import {StyleSheet, View, StatusBar} from 'react-native';
import TrackPlayer, {seekTo} from 'react-native-track-player';
const {usePlaybackState, useTrackPlayerEvents} = TrackPlayer;

// import Player from 'src/components/Player';
// import playlistData from 'src/data/playlist.json';
import {ScrollView} from 'react-native-gesture-handler';
// import Header from 'src/components/Header';
import AlbumArt from 'src/components/AlbumArt';
import {placeholderImage, elevation} from 'src/themes';
import TrackDetail from 'src/components/TrackDetail';
import SeekBar from 'src/components/SeekBar';
import Controls from 'src/components/Controls';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootStackParamList} from 'src/Router';
import {playOrPause} from 'src/utils';
import {withTheme, Appbar, overlay, Colors} from 'react-native-paper';
import {SafeAreaLayout, SaveAreaInset} from 'src/components/SafeAreaLayout';
import {Theme} from 'react-native-paper/lib/typescript/src/types';
// import AppService from 'src/services/AppService';
// import localTrack from 'src/resources/korean.MP3';

type PlayerNavigationProp = StackNavigationProp<RootStackParamList, 'Player'>;
type PlayerRouteProp = RouteProp<RootStackParamList, 'Player'>;

type Props = {
  navigation: PlayerNavigationProp;
  route: PlayerRouteProp;
  theme: Theme;
};

export default withTheme(function PlayerDetail({
  navigation,
  route,
  theme,
}: Props) {
  let {playerState} = route.params;
  // console.log('playerState ', playerState);
  let playbackState = usePlaybackState();
  let firstLoad = useRef(true);
  // console.log('playbackState ', playbackState);
  // console.log('TrackPlayer.STATE_PLAYING ', TrackPlayer.STATE_PLAYING);
  // console.log(
  //   'gg',
  //   !(
  //     playerState === TrackPlayer.STATE_PLAYING ||
  //     playerState === TrackPlayer.STATE_BUFFERING
  //   ),
  // );
  const [trackTitle, setTrackTitle] = useState('');
  const [trackArtwork, setTrackArtwork] = useState('');
  const [trackArtist, setTrackArtist] = useState('');
  const [message, setMessage] = useState(getStateName(playerState));
  let [paused, setPause] = useState(
    !(
      playerState === TrackPlayer.STATE_PLAYING ||
      playerState === TrackPlayer.STATE_BUFFERING
    ),
  );
  useTrackPlayerEvents(['playback-track-changed'], async event => {
    if (event.type === TrackPlayer.TrackPlayerEvents.PLAYBACK_TRACK_CHANGED) {
      const track = await TrackPlayer.getTrack(event.nextTrack);
      if (track) {
        setTrackTitle(track.title);
        setTrackArtist(track.artist);
        setTrackArtwork(track.artwork);
      }
    }
  });

  let effect = useCallback(async () => {
    let currentTrack = await TrackPlayer.getCurrentTrack();
    let track = await TrackPlayer.getTrack(currentTrack);
    if (track) {
      setTrackTitle(track.title);
      setTrackArtist(track.artist);
      setTrackArtwork(track.artwork);
    }
  }, []);

  useEffect(() => {
    effect();
  }, [effect]);

  useEffect(() => {
    // let state = true;
    // switch (playbackState) {
    //   case TrackPlayer.STATE_NONE:
    //     state = true;
    //     break;
    //   case TrackPlayer.STATE_PLAYING:
    //     state = false;
    //     break;
    //   case TrackPlayer.STATE_PAUSED:
    //     state = true;
    //     break;
    //   case TrackPlayer.STATE_STOPPED:
    //     state = true;
    //     break;
    //   case TrackPlayer.STATE_BUFFERING:
    //     state = false;
    //     break;
    // }
    if (firstLoad.current) {
      StatusBar.setBackgroundColor(
        // @ts-ignore
        theme.dark ? overlay(elevation, theme.colors.surface) : Colors.grey400,
        true,
      );
      firstLoad.current = false;
      setPause(
        !(
          playerState === TrackPlayer.STATE_PLAYING ||
          playerState === TrackPlayer.STATE_BUFFERING
        ),
      );
    } else {
      setPause(
        !(
          playbackState === TrackPlayer.STATE_PLAYING ||
          playbackState === TrackPlayer.STATE_BUFFERING
        ),
      );
      setMessage(getStateName(playbackState));
    }
  }, [playbackState, playerState, theme.colors.surface, theme.dark]);

  // const {style, onNext, onPrevious, onTogglePlayback /*stateName*/} = props;

  // var middleButtonText = 'Play';
  // var paused = true;

  // if (
  //   playbackState === TrackPlayer.STATE_PLAYING ||
  //   playbackState === TrackPlayer.STATE_BUFFERING
  // ) {
  //   // middleButtonText = 'Pause';
  //   paused = false;
  // }
  // async function togglePlayback() {
  // const currentTrack = await TrackPlayer.getCurrentTrack();
  // console.log('currentTrack ', currentTrack);
  // if (currentTrack == null) {
  // await TrackPlayer.reset();
  // await TrackPlayer.add({
  //   id: 'from-yt',
  //   url:
  //     'https://r3---sn-apou5n5gu5-jb3l.googlevideo.com/videoplayback?expire=1578047671&ei=V8QOXqWIKaGU3LUP9se-iAI&ip=182.23.2.98&id=o-ADR_HGHVKplDwjYiZaqlmpBfuJbEwt4G6XTyJ7JIMvoe&itag=140&source=youtube&requiressl=yes&mm=31%2C29&mn=sn-apou5n5gu5-jb3l%2Csn-npoe7n76&ms=au%2Crdu&mv=m&mvi=2&pl=24&initcwndbps=253750&mime=audio%2Fmp4&gir=yes&clen=3193771&dur=197.299&lmt=1577634229524050&mt=1578026003&fvip=4&keepalive=yes&fexp=23842630%2C23860862&c=WEB&txp=5531432&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cmime%2Cgir%2Cclen%2Cdur%2Clmt&sig=ALgxI2wwRQIgZGg_7UHr8zB-6gHAwDAJNO8-nR7ODnVYyXcJWyIZSvACIQDdG_HpiRm0PX2w5lq_cCK3kgr4uyk-tOhajZFD7uEMAg%3D%3D&lsparams=mm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl%2Cinitcwndbps&lsig=AHylml4wRAIgFDs2vcG_PaNojdeLneGmV5KzphXpA35RVJO3SZdn1C0CIFxnFPkBFBsdq-p14k33-yIxAYiGGpDHyGqejAQcxKV3&host=r3---sn-apou5n5gu5-jb3l.googlevideo.com',
  //   title: 'G.C.F in Helsinki',
  //   artist: 'BANGTANTV',
  //   artwork: 'https://i.ytimg.com/vi/NgTwJQ2CUqI/mqdefault.jpg',
  // });
  // await TrackPlayer.add(playlistData);
  //   await TrackPlayer.add({
  //     id: 'local-track',
  //     url: require('../resources/korean.MP3'),
  //     title: 'Pure (Demo)',
  //     artist: 'David Chavez',
  //     artwork: 'https://picsum.photos/200',
  //   });
  // await TrackPlayer.play();
  // } else {
  // if (playbackState === TrackPlayer.STATE_PAUSED) {
  //   await TrackPlayer.play();
  // } else {
  //   await TrackPlayer.pause();
  // }
  // let state = firstLoad.current ? playerState : playbackState;
  // console.log('state ', state);
  // console.log('playerState ', playerState);
  // console.log('playbackState ', playbackState);
  // playOrPause(state);
  // if (
  //   state === TrackPlayer.STATE_PLAYING ||
  //   state === TrackPlayer.STATE_BUFFERING
  // ) {
  //   await TrackPlayer.pause();
  // } else {
  //   await TrackPlayer.play();
  // }
  // }
  // }

  // const message = getStateName(playerState) || '';
  // const color = theme.dark ? Colors.black : Colors.white;

  return (
    <SafeAreaLayout
      insets={SaveAreaInset.TOP}
      style={[styles.container, {backgroundColor: theme.colors.background}]}>
      {/* <Header message={message} onDownPress={() => navigation.goBack()} /> */}
      <Appbar
        style={[styles.appbar, {backgroundColor: theme.colors.background}]}>
        <Appbar.Action
          icon="chevron-down"
          onPress={() => navigation.goBack()}
        />
        <Appbar.Content title={message} titleStyle={styles.title} />
        <View style={{flex: 0.3}} />
      </Appbar>
      <ScrollView>
        {/* <Text style={styles.description}>
        We'll be inserting a playlist into the library loaded from
        `playlist.json`. We'll also be using the `ProgressComponent` which
        allows us to track playback time.
      </Text> */}
        {/* <Player
          onNext={skipToNext}
          style={styles.player}
          onPrevious={skipToPrevious}
          onTogglePlayback={togglePlayback}
          stateName={message}
        /> */}
        {/* <Text style={styles.state}>{getStateName(playbackState)}</Text> */}
        <View style={[styles.player]}>
          <AlbumArt url={trackArtwork || placeholderImage} />
          <TrackDetail title={trackTitle} artist={trackArtist} />
          <SeekBar onSeek={seekTo} />
          <Controls
            onPressRepeat={() => {}}
            repeatOn={false}
            shuffleOn={false}
            forwardDisabled={false}
            onPressShuffle={() => {}}
            onTogglePlayback={playOrPause}
            onPrevious={skipToPrevious}
            onForward={skipToNext}
            paused={paused}
          />
        </View>
      </ScrollView>
    </SafeAreaLayout>
  );
});

// PlayerDetail.navigationOptions = {
//   title: 'Playlist Example',
// };

function getStateName(state: string | number) {
  switch (state) {
    case TrackPlayer.STATE_NONE:
      return 'None';
    case TrackPlayer.STATE_PLAYING:
      return 'Playing';
    case TrackPlayer.STATE_PAUSED:
      return 'Paused';
    case TrackPlayer.STATE_STOPPED:
      return 'Stopped';
    case TrackPlayer.STATE_BUFFERING:
      return 'Buffering';
    default:
      return '';
  }
}

// async function getPosition() {
//   try {
//     await TrackPlayer.getPosition();
//   } catch (_) {}
// }

// async function getDuration() {
//   try {
//     await TrackPlayer.getDuration();
//   } catch (_) {}
// }

async function skipToNext() {
  try {
    await TrackPlayer.skipToNext();
  } catch (_) {}
}

async function skipToPrevious() {
  try {
    await TrackPlayer.skipToPrevious();
  } catch (_) {}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: 'rgb(4,4,4)',
    // alignItems: 'center',
    // backgroundColor: '#F5FCFF',
  },
  description: {
    width: '80%',
    marginTop: 20,
    textAlign: 'center',
  },
  player: {
    // marginTop: 40,
  },
  state: {
    marginTop: 20,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 10,
    alignSelf: 'center',
    textTransform: 'uppercase',
  },
  appbar: {
    elevation: 0,
  },
});
