/**@ts-ignore */
import React, {useCallback, useState} from 'react';
import {StyleSheet, FlatList} from 'react-native';
import {List, Divider, Avatar, Appbar} from 'react-native-paper';
import {Layout} from 'src/components/Layout';
import Track from 'src/domains/Track';
import {placeholderImage} from 'src/themes';
import {useFocusEffect} from '@react-navigation/native';
import AppService from 'src/services/AppService';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import {LibraryStackParamList} from 'src/Router';
import {AppBarHeader} from 'src/components/AppBar';

// type PlaylistNavigationProp = StackNavigationProp<
//   LibraryStackParamList,
//   'Playlist'
// >;
// type PlaylistRouteProp = RouteProp<LibraryStackParamList, 'Playlist'>;

type Props = {
  // navigation: PlaylistNavigationProp;
  navigation: object;
};

const Playlist = ({navigation}: Props) => {
  let [tracks, setTrack] = useState<Track[]>([]);
  useFocusEffect(
    useCallback(() => {
      // Do something when the screen is focused
      AppService.trackService.findAllTracks().then(res => {
        let tracks = res.map(obj => {
          let track = new Track();
          return track.create(obj);
        });
        setTrack(tracks);
      });

      return () => {
        // Do something when the screen is unfocused
        // Useful for cleanup functions
      };
    }, []),
  );

  return (
    <Layout style={styles.container}>
      <AppBarHeader>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title="Playlist" />
      </AppBarHeader>
      <FlatList
        style={styles.list}
        data={tracks}
        keyExtractor={item => item.videoId}
        ItemSeparatorComponent={() => <Divider />}
        renderItem={({item, index}) => {
          let thumbnail = item.videoThumbnails.find(
            v => v.quality === 'medium',
          );
          return (
            <List.Item
              onPress={() => {
                navigation.push('TrackDetails', {
                  videoId: item.videoId,
                  data: Track.serialize(new Track().create(item)),
                  request: false,
                });
              }}
              title={item.title}
              left={props => (
                <Avatar.Image
                  {...props}
                  size={50}
                  source={{uri: thumbnail?.url ?? placeholderImage}}
                />
              )}
            />
          );
        }}
      />
    </Layout>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    marginBottom: 50,
  },
});

export default Playlist;
