import React, {useCallback, useState} from 'react';
import {StyleSheet, FlatList, StatusBar} from 'react-native';
import {
  List,
  Divider,
  Avatar,
  Appbar,
  Colors,
  Theme,
  withTheme,
  overlay,
} from 'react-native-paper';
// import Track from 'src/domains/Track';
import {placeholderImage, elevation} from 'src/themes';
import {useFocusEffect} from '@react-navigation/native';
// import AppService from 'src/services/AppService';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import {LibraryStackParamList} from 'src/Router';
import {AppBarHeader} from 'src/components/AppBar';
import Loading from 'src/components/Loading';
import TrackPlayer, {Track} from 'react-native-track-player';
import ProgressBar from 'src/components/ProgressBar';

type QueueNavigationProp = StackNavigationProp<LibraryStackParamList, 'Queue'>;
type QueueRouteProp = RouteProp<LibraryStackParamList, 'Queue'>;

type Props = {
  navigation: QueueNavigationProp;
  route: QueueRouteProp;
  theme: Theme;
};

const Queue = ({navigation, theme}: Props) => {
  let [tracks, setTrack] = useState<Track[]>([]);
  let [currentTrack, setCurrentTrack] = useState<string>('');
  let backgroundColor = theme.dark ? '#182860' : Colors.purple200;
  TrackPlayer.useTrackPlayerEvents(
    [TrackPlayer.TrackPlayerEvents.PLAYBACK_TRACK_CHANGED],
    ev => {
      if (ev.type === TrackPlayer.TrackPlayerEvents.PLAYBACK_TRACK_CHANGED) {
        setCurrentTrack(ev.nextTrack);
      }
    },
  );
  useFocusEffect(
    useCallback(() => {
      // Do something when the screen is focused
      TrackPlayer.getQueue()
        .then(res => {
          setTrack(res);
          return 'next';
        })
        .then(() => TrackPlayer.getCurrentTrack())
        .then(setCurrentTrack);

      return () => {
        // Do something when the screen is unfocused
        // Useful for cleanup functions
      };
    }, []),
  );
  StatusBar.setBackgroundColor(
    // @ts-ignore
    theme.dark
      ? overlay(elevation, theme.colors.surface)
      : theme.colors.primary,
    true,
  );

  return (
    <>
      <AppBarHeader>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title="Now Playing" />
      </AppBarHeader>
      <FlatList
        style={styles.list}
        extraData={currentTrack}
        data={tracks}
        keyExtractor={(_, index) => index.toString()}
        // ItemSeparatorComponent={() => <Divider />}
        ListEmptyComponent={Loading}
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        renderItem={({item}) => {
          let current = item.id === currentTrack;
          return (
            <>
              <List.Item
                style={current && {backgroundColor}}
                title={item.title}
                left={props => (
                  <Avatar.Image
                    {...props}
                    size={50}
                    source={{uri: item.artwork || placeholderImage}}
                  />
                )}
              />
              {current ? <ProgressBar /> : <Divider />}
            </>
          );
        }}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    marginBottom: 50,
  },
  btn: {
    width: 150,
    alignSelf: 'center',
  },
  playing: {
    backgroundColor: Colors.grey300,
  },
});

export default withTheme(Queue);
