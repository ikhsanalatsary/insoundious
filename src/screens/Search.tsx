import React, {useState, useEffect, useRef} from 'react';
import {StyleSheet, StatusBar} from 'react-native';
// import {Layout} from 'src/components/Layout';
import {
  Searchbar,
  Menu,
  IconButton,
  Theme,
  ActivityIndicator,
  overlay,
  withTheme,
  List,
  Divider,
  Colors,
} from 'react-native-paper';
import Track from 'src/domains/Track';
import AppService from 'src/services/AppService';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp, useIsFocused} from '@react-navigation/native';
import {SearchStackParamList} from 'src/Router';
import {FlatGrid} from 'react-native-super-grid';
import TrackItem from 'src/components/TrackItem';
import {elevation} from 'src/themes';
import Autocomplete from 'src/components/AutocompleteInput';
import {useDebounce} from 'use-debounce';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {SafeAreaLayout, SaveAreaInset} from 'src/components/SafeAreaLayout';
import {ERROR_MESSAGE, useAppDispatch, SHOW_SNACK_BAR} from 'src/Context';

type SearchNavigationProp = StackNavigationProp<SearchStackParamList, 'Search'>;
type SearchRouteProp = RouteProp<SearchStackParamList, 'Search'>;
type Props = {
  navigation: SearchNavigationProp;
  theme: Theme;
  route: SearchRouteProp;
};

const {ConnectionState} = AppService;

export default withTheme(function Search({navigation, theme}: Props) {
  let backgroundColor = theme.dark
    ? overlay(elevation, theme.colors.surface)
    : Colors.white;
  let showSnackbar = useAppDispatch();
  let searchBarRef = useRef(null);
  let [value, setValue] = useState('');
  let [debouncedSearchTerm, cancelDebounce] = useDebounce(value, 100);
  let [toggleFilter, setToggleFilter] = useState(false);
  let [filter, setFilter] = useState('all');
  let [conState, setConnectionState] = useState(ConnectionState.NONE);
  let [hideSuggestResult, setHideSuggestResult] = useState(false);
  let [results, setResult] = useState<Track[]>([]);
  let [suggest, setSuggest] = useState<string[]>([]);
  let isFocused = useIsFocused();
  let onFilter = (txt: string) => {
    setFilter(txt);
    setToggleFilter(!toggleFilter);
  };

  let onSubmit = async (val = value) => {
    // @ts-ignore
    // searchBarRef.current?.clear?.();
    setConnectionState(ConnectionState.LOADING);
    setValue(val);
    // cancelDebounce();
    // setSuggest([]);
    setHideSuggestResult(true);
    AppService.getSearch(val, filter)
      .then(res => {
        let tracks = res.map(obj => {
          let track = new Track();
          track.create(obj);
          return track;
        });
        setHideSuggestResult(true);
        setResult(tracks);
        setConnectionState(ConnectionState.SUCCESS);
      })
      .catch(_ => {
        setConnectionState(ConnectionState.ERROR);
        setHideSuggestResult(true);
        showSnackbar({type: SHOW_SNACK_BAR, message: ERROR_MESSAGE});
      });
  };

  let onNavigate = (item: Track) => {
    navigation.push('TrackDetails', {
      videoId: item.videoId,
      data: Track.serialize(new Track().create(item)),
      request: true,
    });
  };

  let onSuggest = (val: string) => {
    AppService.getSuggest(val)
      .then((res: any[]) => {
        // console.log('res ', res);
        let [q, suggests] = res;
        setSuggest(suggests);
        // showSnackbar({type: SHOW_SNACK_BAR, message: 'Success'});
      })
      .catch(() => {
        showSnackbar({type: SHOW_SNACK_BAR, message: ERROR_MESSAGE});
      });
  };

  // Effect for API call
  useEffect(() => {
    if (debouncedSearchTerm) {
      onSuggest(debouncedSearchTerm);
      conState !== ConnectionState.LOADING && setHideSuggestResult(false);
    } else {
      setHideSuggestResult(true);
      setSuggest([]);
    }
  }, [debouncedSearchTerm]); // Only call effect if debounced search term changes

  // console.log(value);
  // console.log(suggest);
  // console.log(isFocused);
  if (isFocused) {
    // @ts-ignore
    !searchBarRef.current?.isFocused?.() && searchBarRef.current?.focus?.();
    StatusBar.setBackgroundColor(
      // @ts-ignore
      theme.dark ? overlay(elevation, theme.colors.surface) : Colors.grey400,
      true,
    );
  }
  return (
    <SafeAreaLayout style={[styles.container]} insets={SaveAreaInset.TOP}>
      <Autocomplete
        onShowResult={() => {}}
        hideResults={hideSuggestResult}
        containerStyle={[styles.container, {zIndex: 1}]}
        // inputContainerStyle={styles.inputContainer}
        listStyle={[
          styles.inputContainer /* {top: 20, position: 'relative'} */,
        ]}
        flatListProps={{ItemSeparatorComponent: () => <Divider />}}
        data={suggest}
        // defaultValue={value}
        // onChangeText={(query: string) => setValue(query)}
        keyExtractor={(item, index) => String(index)}
        // onEndEditing={console.log}
        // @ts-ignore
        renderTextInput={({style, ref, ...rest}) => {
          return (
            <Searchbar
              {...rest}
              // @ts-ignore
              ref={searchBarRef}
              icon={() => (
                <Menu
                  visible={toggleFilter}
                  onDismiss={() => setToggleFilter(false)}
                  anchor={
                    <IconButton
                      icon="filter"
                      onPress={() => setToggleFilter(!toggleFilter)}
                    />
                  }>
                  <Menu.Item onPress={() => onFilter('all')} title="All" />
                  <Menu.Item onPress={() => onFilter('video')} title="Video" />
                  <Menu.Item
                    onPress={() => onFilter('playlist')}
                    title="Playlist"
                  />
                  <Menu.Item
                    onPress={() => onFilter('channel')}
                    title="Channel"
                  />
                </Menu>
              )}
              autoFocus
              placeholder="Search"
              onChangeText={(query: string) => {
                if (query === '') {
                  setResult([]);
                  // @ts-ignore
                  searchBarRef.current?.clear?.();
                }
                setValue(query);
              }}
              value={value}
              style={styles.searchbar}
              onSubmitEditing={() => {
                onSubmit();
              }}
            />
          );
        }}
        renderItem={({item}) => (
          <List.Item
            style={{backgroundColor: theme.colors.background}}
            onPress={() => {
              onSubmit(item);
            }}
            title={item}
            right={props => (
              <Icon {...props} name="arrow-top-right" size={20} />
            )}
          />
        )}
      />
      <FlatGrid
        style={styles.list}
        itemDimension={130}
        items={results}
        listKey="videoId"
        ListEmptyComponent={() =>
          conState === ConnectionState.LOADING ? (
            <ActivityIndicator size="large" animating={true} />
          ) : null
        }
        // ListFooterComponent={() =>
        //   conState === ConnectionState.LOADING ? <ActivityIndicator size="large" animating={true} /> : null
        // }
        renderItem={({item, index}) => {
          return (
            <TrackItem
              item={item}
              backgroundColor={backgroundColor}
              onNavigate={() => {
                onNavigate(item);
              }}
            />
          );
        }}
      />
    </SafeAreaLayout>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  caption: {
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  searchbar: {
    margin: 4,
  },
  list: {
    // marginTop: 50,
    marginBottom: 50, // TODO: change this dynamically
  },
  inputContainer: {
    marginTop: 0,
    borderColor: undefined,
    borderRadius: undefined,
    borderWidth: undefined,
    marginBottom: undefined,
  },
});
