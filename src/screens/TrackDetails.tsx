/* eslint-disable react-native/no-inline-styles */
import React, {useCallback, useState, useEffect, useRef} from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  ScrollView,
  Image,
  StatusBar,
} from 'react-native';
import {
  Appbar,
  withTheme,
  overlay,
  Button,
  Chip,
  List,
  Title,
  Avatar,
  Divider,
  Paragraph,
  Menu,
} from 'react-native-paper';
import {RouteProp, useFocusEffect} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {elevation, placeholderImage} from 'src/themes';
import {
  toggleAddAndPlay,
  formatAdapter,
  PlayPriority,
  hoursAndMinutesAndSeconds,
  numberWithCommas,
  truncateString,
} from 'src/utils';
import AppService from 'src/services/AppService';
import {StackParamList} from 'src/Router';
import TrackPlayer from 'react-native-track-player';
import {AppBarHeader} from 'src/components/AppBar';
import Track from 'src/domains/Track';
import Empty from 'src/components/Empty';
import Loading from 'src/components/Loading';
import {useAppDispatch, SHOW_SNACK_BAR, ERROR_MESSAGE} from 'src/Context';
import {Theme} from 'react-native-paper/lib/typescript/src/types';

const {usePlaybackState} = TrackPlayer;
const {ConnectionState} = AppService;
type TrackDetailsNavigationProp = StackNavigationProp<
  StackParamList,
  'TrackDetails'
>;
type TrackDetailsRouteProp = RouteProp<StackParamList, 'TrackDetails'>;

type Props = {
  navigation: TrackDetailsNavigationProp;
  theme: Theme;
  route: TrackDetailsRouteProp;
};

// const ERRORANDLOADING = [
//   ConnectionState.LOADING,
//   ConnectionState.ERROR,
// ] as const;

export default React.memo(
  withTheme(function TrackDetails({navigation, theme, route}: Props) {
    let {videoId, data, request} = route.params;
    // let backgroundColor = theme.dark
    //   ? overlay(elevation, theme.colors.surface)
    //   : Colors.white;
    let showSnackbar = useAppDispatch();
    let playbackState = usePlaybackState();
    let status =
      playbackState === TrackPlayer.STATE_PLAYING ||
      playbackState === TrackPlayer.STATE_BUFFERING
        ? 'pause'
        : 'play';
    let [connectionState, setConnectionState] = useState(ConnectionState.NONE);
    let [track, setTrack] = useState(new Track().create(data as Track));
    // let [status, setStatus] = useState('play');
    let [toggleFilter, setToggleFilter] = useState(false);
    let firstMounted = useRef(true);
    let [shouldRequest, setShouldRequest] = useState(
      request || data.recommendedVideos.length === 0,
    );
    let [refresh, shouldRefresh] = useState(false);
    let effect = useCallback(() => {
      // Do something when the screen is focused
      console.log('request ll ', shouldRequest);
      if (shouldRequest) {
        !firstMounted.current && setConnectionState(ConnectionState.LOADING);
        // !refresh && setTrack(convertToTrackDetail(data));
        AppService.getTrackDetail(videoId)
          .then(res => {
            let trackDetail = new Track();
            setTrack(
              Object.assign(
                trackDetail,
                track,
                res,
                !res.description ? {description: track.description} : undefined,
                res.authorThumbnails.length === 0
                  ? {authorThumbnails: track.authorThumbnails}
                  : undefined,
              ),
            );
            if (trackDetail.recommendedVideos.length) {
              setConnectionState(ConnectionState.SUCCESS);
            } else {
              setConnectionState(ConnectionState.PARTIAL);
            }
            showSnackbar({type: SHOW_SNACK_BAR, message: 'Success'});
            shouldRefresh(false);
          })
          .catch(e => {
            if (e.name !== 'AbortError') {
              setConnectionState(ConnectionState.ERROR);
              showSnackbar({type: SHOW_SNACK_BAR, message: ERROR_MESSAGE});
              shouldRefresh(false);
            }
          });
      } else {
        setConnectionState(ConnectionState.SUCCESS);
        shouldRefresh(false);
      }

      return () => {
        // Do something when the screen is unfocused
        // Useful for cleanup functions
      };
    }, [videoId, shouldRequest]);

    useEffect(() => {
      (firstMounted.current || refresh) && effect();
      if (firstMounted.current) {
        firstMounted.current = false;
        // effect();
        console.log('vv');
      }

      console.log('llll');
    }, [effect, refresh]);

    useFocusEffect(
      useCallback(() => {
        StatusBar.setBackgroundColor(
          // @ts-ignore
          theme.dark
            ? overlay(elevation, theme.colors.surface)
            : theme.colors.primary,
          true,
        );
      }, []),
    );

    // useEffect(() => {
    //   // @ts-ignore
    //   if (!ERRORANDLOADING.includes(connectionState)) {
    //     setStatus(
    //       playbackState === TrackPlayer.STATE_PLAYING ? 'pause' : 'play',
    //     );
    //     console.log('kkkkk');
    //   }
    //   return () => {};
    // }, [playbackState]);

    // useFocusEffect(useCallback(refetch, [videoId]));

    // console.log('hasData? ', track.hasData);
    // console.log('track? ', JSON.stringify(track.authorThumbnails, null, 2));
    // console.log('connectionState? ', connectionState);
    // console.log('firstMounted? ', firstMounted.current);

    const header = (
      <AppBarHeader>
        <Appbar.BackAction
          onPress={() => {
            AppService.abortController?.abort();
            navigation.pop();
          }}
        />
        <Appbar.Content title={track.title} />
        <Menu
          visible={toggleFilter}
          onDismiss={() => setToggleFilter(false)}
          anchor={
            <Appbar.Action
              icon="dots-vertical"
              onPress={() => setToggleFilter(!toggleFilter)}
              color="white"
            />
          }>
          <Menu.Item
            onPress={() => {
              AppService.trackService.deleteTrackById(track.videoId);
              setToggleFilter(false);
            }}
            title="Delete Cache"
          />
          <Menu.Item
            onPress={() => {
              // setConnectionState(ConnectionState.LOADING);
              if (!shouldRequest) {
                setShouldRequest(true);
              }
              shouldRefresh(true);
              setToggleFilter(false);
            }}
            title="Refresh"
          />
        </Menu>
      </AppBarHeader>
    );

    if (connectionState === ConnectionState.LOADING) {
      return (
        <>
          {header}
          {Loading()}
        </>
      );
    } else if (connectionState === ConnectionState.ERROR) {
      return (
        <>
          {header}
          <Empty
            onPress={() => {
              // setConnectionState(ConnectionState.LOADING);
              shouldRefresh(true);
            }}
          />
        </>
      );
    } else {
      const elapsed = hoursAndMinutesAndSeconds(track.lengthSeconds);
      let time = elapsed[0] + ':' + elapsed[1] + ':' + elapsed[2];
      if (elapsed[0] === '00') {
        elapsed.shift();
        time = elapsed[0] + ':' + elapsed[1];
      }
      // console.log('elapsed, ', elapsed);
      let artwork = track.videoThumbnails.find(v => v.quality === 'medium');
      let audio = formatAdapter(track.adaptiveFormats, true); // TODO: best bitrate should be from setting
      // console.log(JSON.stringify(track, null, 2));

      return (
        <>
          {header}
          <ScrollView>
            <Image
              resizeMode="cover"
              source={{uri: artwork?.url}}
              style={styles.artwork}
            />
            <View style={styles.row}>
              <Chip
                icon="account-circle"
                style={styles.chip}
                onPress={() =>
                  navigation.navigate('PlayByArtist', {
                    author: track.author,
                    authorId: track.authorId,
                  })
                }>
                {truncateString(track.author, 14)}
              </Chip>
              <Chip icon="clock" style={styles.chip}>
                {time}
              </Chip>
              <Chip icon="eye" style={styles.chip}>
                {numberWithCommas(track.viewCount)}
              </Chip>
            </View>
            <Paragraph style={styles.paragraph}>{track.description}</Paragraph>
            <Button
              style={[styles.title, styles.refresh]}
              icon={status}
              mode="contained"
              // theme={{roundness: 20}}
              onPress={async () => {
                try {
                  if (audio && audio.url) {
                    if (shouldRequest) {
                      await AppService.trackService.findByVideoIdOrCreate(
                        data.videoId,
                        Object.assign(new Track(), data, track),
                      );
                      console.log('success save to the db');
                    }
                    toggleAddAndPlay(
                      {
                        id: track.videoId,
                        // url: track.adaptiveFormats[0].url,
                        url: audio?.url,
                        artist: track.author,
                        title: track.title,
                        duration: track.lengthSeconds,
                        artwork: artwork?.url ?? placeholderImage,
                      },
                      status === 'play' ? PlayPriority.NOW : PlayPriority.LATER,
                    );
                  }
                } catch (e) {
                  console.log('error sini', e);
                }
              }}>
              {status.toUpperCase()}
            </Button>
            <Button
              style={[styles.title, styles.refresh]}
              icon="plus"
              mode="contained"
              // theme={{roundness: 20}}
              onPress={async () => {
                try {
                  if (audio && audio.url) {
                    if (shouldRequest) {
                      await AppService.trackService.findByVideoIdOrCreate(
                        data.videoId,
                        Object.assign(new Track(), data, track),
                      );
                      console.log('success save to the db d');
                    }
                    TrackPlayer.add({
                      id: track.videoId,
                      // url: track.adaptiveFormats[0].url,
                      url: audio?.url,
                      artist: track.author,
                      title: track.title,
                      duration: track.lengthSeconds,
                      artwork: artwork?.url ?? placeholderImage,
                    });
                  }
                } catch (error) {
                  console.log('error sini sana', error);
                }
              }}>
              Add to queue
            </Button>
            <Title style={styles.title}>You might also like</Title>
            <FlatList
              style={styles.list}
              scrollEnabled={false}
              data={track.recommendedVideos}
              keyExtractor={item => item.videoId}
              ListEmptyComponent={() =>
                connectionState === ConnectionState.PARTIAL ? (
                  <Empty
                    onPress={() => {
                      // setConnectionState(ConnectionState.LOADING);
                      shouldRefresh(true);
                    }}
                  />
                ) : (
                  Loading()
                )
              }
              ItemSeparatorComponent={() => <Divider />}
              renderItem={({item, index}) => {
                let thumbnail = item.videoThumbnails.find(
                  v => v.quality === 'medium',
                );
                return (
                  <List.Item
                    onPress={() => {
                      console.log('push');
                      navigation.push('TrackDetails', {
                        videoId: item.videoId,
                        data: Track.serialize(new Track().create(item)),
                        request: true,
                      });
                    }}
                    title={item.title}
                    left={props => (
                      <Avatar.Image
                        {...props}
                        size={50}
                        source={{uri: thumbnail?.url ?? placeholderImage}}
                      />
                    )}
                  />
                );
              }}
            />
          </ScrollView>
        </>
      );
    }
  }),
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  artwork: {
    width: 150,
    height: 150,
    alignSelf: 'center',
    marginTop: 10,
  },
  row: {
    margin: 5,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  chip: {
    marginVertical: 8,
  },
  title: {
    alignSelf: 'center',
  },
  refresh: {
    margin: 8,
  },
  align: {alignItems: 'center'},
  list: {
    marginBottom: 50,
  },
  paragraph: {
    paddingHorizontal: 8,
  },
});
