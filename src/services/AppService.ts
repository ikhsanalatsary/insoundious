import RestService from './RestService';
import Config from 'react-native-config';
import {ITrack} from 'src/domains/Track';
// import {ITrackDetail} from 'src/domains/TrackDetail';
import {stringify} from 'query-string';
import TrackPlayer from 'react-native-track-player';
import ModelProvider from 'src/model/ModelProvider';
import TrackRepository from 'src/repository/TrackRepository';
import TrackService from './TrackService';
import StorageService from './StorageService';

export interface ICountry {
  countryCode: string;
  countryCode3: string;
  countryName: string;
  countryEmoji: string;
}

enum ConnectionState {
  NONE,
  LOADING,
  SUCCESS,
  ERROR,
  PARTIAL,
}

const rest = RestService(Config.BASE_URL);
// const gApi = RestService(Config.SUGGEST_URL);
const _trackRepository = new TrackRepository(ModelProvider.getInstance());
const _trackService = new TrackService(_trackRepository);

const getTrending = async () => {
  const val = await getCountry();

  return rest.get<ITrack[]>(
    `api/v1/trending?type=music&region=${val.countryCode}&hl=en-US`,
  );
};

const getPublishedByArtist = (authorId: string) =>
  rest
    .get<{latestVideos: ITrack[]}>(`api/v1/channels/${authorId}`)
    .then(res => res.latestVideos);

const getTrackDetail = (videoId: string) => {
  let opts = {
    // fields: [
    //   'videoId',
    //   'title',
    //   'author',
    //   'viewCount',
    //   'likeCount',
    //   'genre',
    //   'rating',
    //   'adaptiveFormats',
    //   'formatStreams',
    //   'recommendedVideos',
    //   'videoThumbnails',
    //   'lengthSeconds',
    //   'description',
    // ],
    // pretty: '1',
    region: 'ID',
  };
  return rest.get<ITrack>(
    `api/v1/videos/${videoId}?${stringify(opts, {arrayFormat: 'comma'})}`,
  );
};

const getSearch = (q: string, type = 'all') =>
  rest.get<ITrack[]>(`api/v1/search?type=${type}&q=${q}`);

const getSuggest = (q: string) =>
  rest.get<any[]>(
    `complete/search?client=firefox&ds=yt&q=${q}`,
    Config.SUGGEST_URL,
    {
      'X-Requested-With': 'XMLHttpRequest',
    },
  );

const getCountry = async (): Promise<ICountry> => {
  const val = await rest.api.get('', {prefixUrl: Config.IPFY}).text();

  return await rest.get<ICountry>(`ip?${val}`, Config.IP2COUNTRY);
};

const playerListener = async function() {
  TrackPlayer.addEventListener('remote-play', () => {
    TrackPlayer.play();
  });

  TrackPlayer.addEventListener('remote-pause', () => {
    TrackPlayer.pause();
  });

  TrackPlayer.addEventListener('remote-next', () => {
    TrackPlayer.skipToNext();
  });

  TrackPlayer.addEventListener('remote-previous', () => {
    TrackPlayer.skipToPrevious();
  });

  TrackPlayer.addEventListener('remote-stop', () => {
    TrackPlayer.destroy();
  });
};

export default {
  ConnectionState,
  getPublishedByArtist,
  getCountry,
  getTrending,
  getTrackDetail,
  getSearch,
  getSuggest,
  playerListener,
  get openDatabase() {
    return ModelProvider.getInstance().open();
  },
  get closeDatabase() {
    return ModelProvider.getInstance().close();
  },
  get trackService() {
    return _trackService;
  },
  get abortController() {
    return rest.abortController;
  },
  get storageService() {
    return StorageService;
  },
};
