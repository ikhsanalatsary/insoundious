import ky, {HTTPError, TimeoutError, RetryOptions, Options} from 'ky';

interface MyTimeoutError extends TimeoutError {
  response: void;
}

class RestService {
  protected baseUrl: string;
  api: typeof ky;
  private _abortController: AbortController | undefined;
  private _retry: RetryOptions | number = 5;
  constructor(baseUrl: string, timeout = 60000) {
    this.baseUrl = baseUrl;
    this.api = ky.create({prefixUrl: this.baseUrl, timeout});
  }

  get<T>(url: string, prefixUrl?: string, headers?: RequestInit['headers']) {
    // console.log('retry ', this.retry);
    // console.log('url ', url);
    // console.log('url ', prefixUrl);
    let abortController = new AbortController();
    this.abortController = abortController;
    let opts: Options = {};
    opts.retry = this.retry;
    opts.headers = headers;
    opts.signal = this.abortController.signal;
    if (prefixUrl) {
      opts.prefixUrl = prefixUrl;
    }
    return this.api
      .get(url, opts)
      .json<T>()
      .catch((e: HTTPError | MyTimeoutError) => {
        console.log(
          JSON.stringify(
            `You got ${e.name}: ${JSON.stringify(e.response, null, 2) || e}`,
          ),
        );
        return Promise.reject(e);
      });
  }

  get retry() {
    return this._retry;
  }

  set retry(val: RetryOptions | number) {
    this._retry = val;
  }

  get abortController() {
    return this._abortController;
  }

  set abortController(val: AbortController | undefined) {
    this._abortController = val;
  }
}

// factory
export default function(baseUrl: string, timeout?: number) {
  return new RestService(baseUrl, timeout);
}
