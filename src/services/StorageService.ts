import AsyncStorage from '@react-native-community/async-storage';
import Setting from 'src/domains/Setting';

export default {
  get name() {
    return '_insoundious_settings_';
  },
  get storage() {
    return AsyncStorage;
  },
  async getSetting(): Promise<Setting> {
    const res = await this.storage.getItem(this.name);
    if (res) {
      let val: Setting = JSON.parse(res);
      return new Setting().create(val);
    }
    return new Setting();
  },
  setSetting(data: Setting) {
    return this.storage.setItem(this.name, JSON.stringify(data));
  },
};
