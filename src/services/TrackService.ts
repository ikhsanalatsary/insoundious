/**
 *
 * @format
 * @flow
 */

import Track from 'src/domains/Track';
import TrackRepository from 'src/repository/TrackRepository';
import {NextTrack} from 'src/domains/TrackDetail';

export default class TrackService {
  repository: TrackRepository;

  constructor(trackRepository: TrackRepository) {
    this.repository = trackRepository;
  }

  createTrack(data: Track | NextTrack) {
    return this.repository.create(data);
  }

  findAllTracks(params: {limit?: number} = {}) {
    return this.repository.findAll(params);
  }

  updateTrackById(id: number, data: Track) {
    return this.repository.update(id, data);
  }

  findTrackById(id: number) {
    return this.repository.findOne(id);
  }

  async findByVideoIdOrCreate(id: string, data: Track) {
    const res = await this.repository.findOneByVideoId(id);
    if (!res) {
      return this.createTrack(data);
    }
    console.log('update');
    return this.updateTrackById(res.id, data);
  }

  async deleteTrackById(id: string) {
    const res = await this.repository.findOneByVideoId(id);
    if (res) {
      return this.repository.delete(res?.id);
    }

    return true;
  }

  findAllArtists(params?: {limit?: number}): Promise<Track[]> {
    return this.repository.findAllArtists(params);
  }

  countRecord(where: Partial<Track> = {}) {
    return this.repository.count(where);
  }

  reset() {
    return this.repository.deleteAll();
  }
}
