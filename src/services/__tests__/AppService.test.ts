import AppService from '../AppService';

jest.setTimeout(60000);
describe('AppService', () => {
  test('should works', async () => {
    const trending = await AppService.getTrending();

    console.log({trending});
    expect(trending).toBeDefined();
  });
});
