// import * as Colors from './Colors';
import {Colors, DefaultTheme, DarkTheme, overlay} from 'react-native-paper';
import {
  DefaultTheme as NavTheme,
  DarkTheme as NavDarkTheme,
} from '@react-navigation/native';

export const elevation = 4;
export const theme = {
  ...DefaultTheme,
  roundness: 2,
  elevation,
  colors: {
    ...DefaultTheme.colors,
    // primary: '#3498db',
    // accent: '#f1c40f',
    primary: '#203878',
    accent: '#f8c018',
  },
};

export const darkTheme = {
  ...DarkTheme,
  roundness: 2,
  elevation,
  colors: {
    ...DarkTheme.colors,
    primary: '#3498db',
    accent: '#f1c40f',
  },
};

export const navTheme = {
  ...NavTheme,
  colors: {
    ...NavTheme.colors,
    primary: '#203878',
    accent: '#f8c018',
    border: Colors.white,
    background: Colors.white,
  },
};

export const navDarkTheme = {
  ...NavDarkTheme,
  colors: {
    ...NavDarkTheme.colors,
    primary: '#3498db',
    accent: '#f1c40f',
    background: overlay(elevation, darkTheme.colors.surface),
    border: darkTheme.colors.surface,
  },
};

// export const placeholderImage = 'https://via.placeholder.com/150';
export const placeholderImage =
  'https://www.larrymitchell.com/stringtime/wp-content/uploads/2018/05/cd.gif';

export const placeholderAuthor =
  'https://github.com/ikhsanalatsary/kontakplus/blob/master/client/uploads/pria.png?raw=true';
