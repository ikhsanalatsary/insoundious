import { AdaptiveFormat } from 'src/domains/TrackDetail';

export function formatAdapter(
  adaptiveFormats: AdaptiveFormat[],
  best: boolean,
): AdaptiveFormat | undefined {
  let formats = adaptiveFormats
    .filter(val => val.type.match(/audio/gi) !== null)
    .sort((a, b) => Number(a.bitrate) - Number(b.bitrate));

  if (formats.length === 0) {
    return undefined;
  }

  // console.log('formats ', JSON.stringify(formats, null, 2));

  return best ? formats[formats.length - 1] : formats[0];
}
