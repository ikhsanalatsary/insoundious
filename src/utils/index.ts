export {default as pad} from './pad';
export {
  minutesAndSeconds,
  hoursAndMinutesAndSeconds,
} from './minutesAndSeconds';
export {toggleAddAndPlay, playOrPause, PlayPriority} from './playback';
export {default as truncateString} from './truncateString';
export {formatAdapter} from './formatAdapter';

export function numberWithCommas(x: number) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}
