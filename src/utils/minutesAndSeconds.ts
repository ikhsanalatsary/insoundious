import pad from './pad';

export const minutesAndSeconds = (position: number) => [
  pad(Math.floor(position / 60), 2),
  pad(Math.floor(position % 60), 2),
];

export const hoursAndMinutesAndSeconds = (position: number) => [
  pad(Math.floor(position / 3600), 2),
  pad(Math.floor(position / 60), 2),
  pad(Math.floor(position % 60), 2),
];
