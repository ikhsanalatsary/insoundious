export default function pad(n: number, width: number, z = 0) {
  let _n = String(n);
  return _n.length >= width
    ? _n
    : new Array(width - _n.length + 1).join(String(z)) + _n;
}
