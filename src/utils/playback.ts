import TrackPlayer, {Track, State} from 'react-native-track-player';

export enum PlayPriority {
  NOW = 'NOW',
  LATER = 'LATER',
}

export async function toggleAddAndPlay(
  playlistData: Track,
  priority = PlayPriority.LATER,
) {
  let track: Track | null = await TrackPlayer.getTrack(playlistData.id);
  if (priority === PlayPriority.NOW) {
    console.log('haha');
    await TrackPlayer.reset();
    track = null;
  }
  if (!track) {
    await TrackPlayer.add(playlistData);
  }
  let playbackState = await TrackPlayer.getState();

  console.log('playbackState ', playbackState);
  console.log('TrackPlayer.STATE_PAUSED ', TrackPlayer.STATE_PAUSED);
  console.log('TrackPlayer.STATE_NONE ', TrackPlayer.STATE_NONE);
  console.log('TrackPlayer.STATE_STOPPED ', TrackPlayer.STATE_STOPPED);
  console.log('TrackPlayer.STATE_READY ', TrackPlayer.STATE_READY);
  console.log('TrackPlayer.STATE_BUFFERING ', TrackPlayer.STATE_BUFFERING);
  switch (playbackState) {
    // case TrackPlayer.STATE_NONE:
    // case TrackPlayer.STATE_PAUSED:
    // case TrackPlayer.STATE_STOPPED:
    //   await TrackPlayer.play();
    //   break;
    case TrackPlayer.STATE_PLAYING:
      await TrackPlayer.pause();
      break;

    default:
      await TrackPlayer.play();
      break;
  }
}

export async function playOrPause() {
  let playbackState: State = await TrackPlayer.getState();

  if (
    playbackState === TrackPlayer.STATE_PLAYING ||
    playbackState === TrackPlayer.STATE_BUFFERING
  ) {
    await TrackPlayer.pause();
  } else if (
    playbackState === TrackPlayer.STATE_PAUSED ||
    playbackState === TrackPlayer.STATE_STOPPED
  ) {
    await TrackPlayer.play();
  }
}
