export default function truncateString(str: string, max: number) {
  // If the length of str is less than or equal to max
  // just return str--don't truncate it.
  if (str.length <= max) {
    return str;
  }
  // Return str truncated with '...' concatenated to the end of str.
  return str.slice(0, max - 3) + '...';
}
