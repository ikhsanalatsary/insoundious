/**
 * https://www.typescriptlang.org/docs/handbook/declaration-merging.html#module-augmentation
 */
import {
  useTrackPlayerProgress,
  usePlaybackState,
  useTrackPlayerEvents,
  useInterval,
  useWhenPlaybackStateChanges,
  usePlaybackStateIs,
} from 'react-native-track-player';
import ky from 'ky';
import {Animated} from 'react-native';

// this is hack
declare module 'react-native' {
  export namespace Animated {
    export interface Value {
      __getValue(): number;
    }
  }
}

declare module 'react-native-track-player' {
  export enum TrackPlayerEvents {
    REMOTE_PLAY = 'remote-play',
    REMOTE_PLAY_ID = 'remote-play-id',
    REMOTE_PLAY_SEARCH = 'remote-play-search',
    REMOTE_PAUSE = 'remote-pause',
    REMOTE_STOP = 'remote-stop',
    REMOTE_SKIP = 'remote-skip',
    REMOTE_PREVIOUS = 'remote-previous',
    REMOTE_SEEK = 'remote-seek',
    REMOTE_SET_RATING = 'remote-set-rating',
    REMOTE_JUMP_FORWARD = 'remote-jump-forward',
    REMOTE_JUMP_BACKWARD = 'remote-jump-backward',
    REMOTE_DUCK = 'remote-duck',
    REMOTE_LIKE = 'remote-like',
    REMOTE_DISLIKE = 'remote-dislike',
    REMOTE_BOOKMARK = 'remote-bookmark',
    PLAYBACK_STATE = 'playback-state',
    PLAYBACK_TRACK_CHANGED = 'playback-track-changed',
    PLAYBACK_QUEUE_ENDED = 'playback-queue-ended',
    PLAYBACK_ERROR = 'playback-error',
  }

  // Hooks
  export function usePlaybackState(): State;
  export function useTrackPlayerEvents(
    events: string[],
    handler: (event: {
      type: EventType;
      nextTrack: string;
      [key: string]: any;
    }) => void,
  ): void;
  export function useInterval(callback: () => void, delay: number): void;
  export function useWhenPlaybackStateChanges(callback: () => void): void;
  export function usePlaybackStateIs(...states: string[]): boolean;
  export function useTrackPlayerProgress(
    interval?: number,
  ): ProgressComponentState;
}

declare module 'ky' {
  export type searchParams =
    | string
    | {[key: string]: string | number | boolean}
    | Array<Array<string | number | boolean>>
    | URLSearchParams;
}
